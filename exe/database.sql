--
-- PostgreSQL database dump
--

-- Dumped from database version 12.5 (Ubuntu 12.5-1.pgdg16.04+1)
-- Dumped by pg_dump version 12.2

-- Started on 2021-01-14 02:44:15

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 2 (class 3079 OID 35463289)
-- Name: postgis; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS postgis WITH SCHEMA public;


--
-- TOC entry 5407 (class 0 OID 0)
-- Dependencies: 2
-- Name: EXTENSION postgis; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION postgis IS 'PostGIS geometry, geography, and raster spatial types and functions';


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 233 (class 1259 OID 35464984)
-- Name: comment; Type: TABLE; Schema: public; Owner: bnrggdvhxaybke
--

CREATE TABLE public.comment (
    id_comment integer NOT NULL,
    id_mission integer NOT NULL,
    id_rescuer integer NOT NULL,
    comment character varying NOT NULL
);


ALTER TABLE public.comment OWNER TO bnrggdvhxaybke;

--
-- TOC entry 232 (class 1259 OID 35464982)
-- Name: comment_id_comment_seq; Type: SEQUENCE; Schema: public; Owner: bnrggdvhxaybke
--

CREATE SEQUENCE public.comment_id_comment_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.comment_id_comment_seq OWNER TO bnrggdvhxaybke;

--
-- TOC entry 5408 (class 0 OID 0)
-- Dependencies: 232
-- Name: comment_id_comment_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bnrggdvhxaybke
--

ALTER SEQUENCE public.comment_id_comment_seq OWNED BY public.comment.id_comment;


--
-- TOC entry 229 (class 1259 OID 35464940)
-- Name: mission; Type: TABLE; Schema: public; Owner: bnrggdvhxaybke
--

CREATE TABLE public.mission (
    id_mission integer NOT NULL,
    description character varying NOT NULL,
    id_station integer NOT NULL
);


ALTER TABLE public.mission OWNER TO bnrggdvhxaybke;

--
-- TOC entry 228 (class 1259 OID 35464938)
-- Name: mission_id_mission_seq; Type: SEQUENCE; Schema: public; Owner: bnrggdvhxaybke
--

CREATE SEQUENCE public.mission_id_mission_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.mission_id_mission_seq OWNER TO bnrggdvhxaybke;

--
-- TOC entry 5409 (class 0 OID 0)
-- Dependencies: 228
-- Name: mission_id_mission_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bnrggdvhxaybke
--

ALTER SEQUENCE public.mission_id_mission_seq OWNED BY public.mission.id_mission;


--
-- TOC entry 231 (class 1259 OID 35464956)
-- Name: mission_task; Type: TABLE; Schema: public; Owner: bnrggdvhxaybke
--

CREATE TABLE public.mission_task (
    id_mission_task integer NOT NULL,
    id_mission integer NOT NULL,
    isfinished boolean NOT NULL,
    id_transportation_type integer NOT NULL,
    id_task_type integer NOT NULL,
    id_rescuer integer
);


ALTER TABLE public.mission_task OWNER TO bnrggdvhxaybke;

--
-- TOC entry 230 (class 1259 OID 35464954)
-- Name: mission_task_id_mission_task_seq; Type: SEQUENCE; Schema: public; Owner: bnrggdvhxaybke
--

CREATE SEQUENCE public.mission_task_id_mission_task_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.mission_task_id_mission_task_seq OWNER TO bnrggdvhxaybke;

--
-- TOC entry 5410 (class 0 OID 0)
-- Dependencies: 230
-- Name: mission_task_id_mission_task_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bnrggdvhxaybke
--

ALTER SEQUENCE public.mission_task_id_mission_task_seq OWNED BY public.mission_task.id_mission_task;


--
-- TOC entry 227 (class 1259 OID 35464919)
-- Name: station; Type: TABLE; Schema: public; Owner: bnrggdvhxaybke
--

CREATE TABLE public.station (
    id_station integer NOT NULL,
    name character varying NOT NULL,
    geo_location public.geography(Point,4326),
    id_lead_rescuer integer
);


ALTER TABLE public.station OWNER TO bnrggdvhxaybke;

--
-- TOC entry 226 (class 1259 OID 35464917)
-- Name: station_id_station_seq; Type: SEQUENCE; Schema: public; Owner: bnrggdvhxaybke
--

CREATE SEQUENCE public.station_id_station_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.station_id_station_seq OWNER TO bnrggdvhxaybke;

--
-- TOC entry 5411 (class 0 OID 0)
-- Dependencies: 226
-- Name: station_id_station_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bnrggdvhxaybke
--

ALTER SEQUENCE public.station_id_station_seq OWNED BY public.station.id_station;


--
-- TOC entry 219 (class 1259 OID 35464870)
-- Name: task_type; Type: TABLE; Schema: public; Owner: bnrggdvhxaybke
--

CREATE TABLE public.task_type (
    id_task_type integer NOT NULL,
    task_name character varying NOT NULL
);


ALTER TABLE public.task_type OWNER TO bnrggdvhxaybke;

--
-- TOC entry 218 (class 1259 OID 35464868)
-- Name: task_type_id_task_type_seq; Type: SEQUENCE; Schema: public; Owner: bnrggdvhxaybke
--

CREATE SEQUENCE public.task_type_id_task_type_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.task_type_id_task_type_seq OWNER TO bnrggdvhxaybke;

--
-- TOC entry 5412 (class 0 OID 0)
-- Dependencies: 218
-- Name: task_type_id_task_type_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bnrggdvhxaybke
--

ALTER SEQUENCE public.task_type_id_task_type_seq OWNED BY public.task_type.id_task_type;


--
-- TOC entry 234 (class 1259 OID 35465003)
-- Name: transportation_eligibility; Type: TABLE; Schema: public; Owner: bnrggdvhxaybke
--

CREATE TABLE public.transportation_eligibility (
    id_rescuer integer NOT NULL,
    id_transportation_type integer NOT NULL
);


ALTER TABLE public.transportation_eligibility OWNER TO bnrggdvhxaybke;

--
-- TOC entry 223 (class 1259 OID 35464892)
-- Name: transportation_type; Type: TABLE; Schema: public; Owner: bnrggdvhxaybke
--

CREATE TABLE public.transportation_type (
    id_transportation_type integer NOT NULL,
    transportation_name character varying NOT NULL,
    visibility_radius integer NOT NULL
);


ALTER TABLE public.transportation_type OWNER TO bnrggdvhxaybke;

--
-- TOC entry 222 (class 1259 OID 35464890)
-- Name: transportation_type_id_transportation_type_seq; Type: SEQUENCE; Schema: public; Owner: bnrggdvhxaybke
--

CREATE SEQUENCE public.transportation_type_id_transportation_type_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.transportation_type_id_transportation_type_seq OWNER TO bnrggdvhxaybke;

--
-- TOC entry 5413 (class 0 OID 0)
-- Dependencies: 222
-- Name: transportation_type_id_transportation_type_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bnrggdvhxaybke
--

ALTER SEQUENCE public.transportation_type_id_transportation_type_seq OWNED BY public.transportation_type.id_transportation_type;


--
-- TOC entry 221 (class 1259 OID 35464881)
-- Name: user_role; Type: TABLE; Schema: public; Owner: bnrggdvhxaybke
--

CREATE TABLE public.user_role (
    id_role integer NOT NULL,
    role character varying NOT NULL
);


ALTER TABLE public.user_role OWNER TO bnrggdvhxaybke;

--
-- TOC entry 220 (class 1259 OID 35464879)
-- Name: user_role_id_role_seq; Type: SEQUENCE; Schema: public; Owner: bnrggdvhxaybke
--

CREATE SEQUENCE public.user_role_id_role_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_role_id_role_seq OWNER TO bnrggdvhxaybke;

--
-- TOC entry 5414 (class 0 OID 0)
-- Dependencies: 220
-- Name: user_role_id_role_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bnrggdvhxaybke
--

ALTER SEQUENCE public.user_role_id_role_seq OWNED BY public.user_role.id_role;


--
-- TOC entry 225 (class 1259 OID 35464903)
-- Name: users; Type: TABLE; Schema: public; Owner: bnrggdvhxaybke
--

CREATE TABLE public.users (
    id_user integer NOT NULL,
    username character varying NOT NULL,
    password character varying NOT NULL,
    photo bytea,
    firstname character varying NOT NULL,
    lastname character varying NOT NULL,
    id_role integer NOT NULL,
    email character varying NOT NULL,
    phonenumber character varying NOT NULL,
    geo_location public.geography(Point,4326),
    approved boolean NOT NULL,
    id_station_member integer
);


ALTER TABLE public.users OWNER TO bnrggdvhxaybke;

--
-- TOC entry 224 (class 1259 OID 35464901)
-- Name: users_id_user_seq; Type: SEQUENCE; Schema: public; Owner: bnrggdvhxaybke
--

CREATE SEQUENCE public.users_id_user_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_user_seq OWNER TO bnrggdvhxaybke;

--
-- TOC entry 5415 (class 0 OID 0)
-- Dependencies: 224
-- Name: users_id_user_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bnrggdvhxaybke
--

ALTER SEQUENCE public.users_id_user_seq OWNED BY public.users.id_user;


--
-- TOC entry 5217 (class 2604 OID 35464987)
-- Name: comment id_comment; Type: DEFAULT; Schema: public; Owner: bnrggdvhxaybke
--

ALTER TABLE ONLY public.comment ALTER COLUMN id_comment SET DEFAULT nextval('public.comment_id_comment_seq'::regclass);


--
-- TOC entry 5215 (class 2604 OID 35464943)
-- Name: mission id_mission; Type: DEFAULT; Schema: public; Owner: bnrggdvhxaybke
--

ALTER TABLE ONLY public.mission ALTER COLUMN id_mission SET DEFAULT nextval('public.mission_id_mission_seq'::regclass);


--
-- TOC entry 5216 (class 2604 OID 35464959)
-- Name: mission_task id_mission_task; Type: DEFAULT; Schema: public; Owner: bnrggdvhxaybke
--

ALTER TABLE ONLY public.mission_task ALTER COLUMN id_mission_task SET DEFAULT nextval('public.mission_task_id_mission_task_seq'::regclass);


--
-- TOC entry 5214 (class 2604 OID 35464922)
-- Name: station id_station; Type: DEFAULT; Schema: public; Owner: bnrggdvhxaybke
--

ALTER TABLE ONLY public.station ALTER COLUMN id_station SET DEFAULT nextval('public.station_id_station_seq'::regclass);


--
-- TOC entry 5210 (class 2604 OID 35464873)
-- Name: task_type id_task_type; Type: DEFAULT; Schema: public; Owner: bnrggdvhxaybke
--

ALTER TABLE ONLY public.task_type ALTER COLUMN id_task_type SET DEFAULT nextval('public.task_type_id_task_type_seq'::regclass);


--
-- TOC entry 5212 (class 2604 OID 35464895)
-- Name: transportation_type id_transportation_type; Type: DEFAULT; Schema: public; Owner: bnrggdvhxaybke
--

ALTER TABLE ONLY public.transportation_type ALTER COLUMN id_transportation_type SET DEFAULT nextval('public.transportation_type_id_transportation_type_seq'::regclass);


--
-- TOC entry 5211 (class 2604 OID 35464884)
-- Name: user_role id_role; Type: DEFAULT; Schema: public; Owner: bnrggdvhxaybke
--

ALTER TABLE ONLY public.user_role ALTER COLUMN id_role SET DEFAULT nextval('public.user_role_id_role_seq'::regclass);


--
-- TOC entry 5213 (class 2604 OID 35464906)
-- Name: users id_user; Type: DEFAULT; Schema: public; Owner: bnrggdvhxaybke
--

ALTER TABLE ONLY public.users ALTER COLUMN id_user SET DEFAULT nextval('public.users_id_user_seq'::regclass);


--
-- TOC entry 5398 (class 0 OID 35464984)
-- Dependencies: 233
-- Data for Name: comment; Type: TABLE DATA; Schema: public; Owner: bnrggdvhxaybke
--

COPY public.comment (id_comment, id_mission, id_rescuer, comment) FROM stdin;
1	1	2	I'm on my way. Will cover the north side first.
2	1	3	I found traces of walking. Need help on the north-east side.
3	1	2	Roger that.
\.


--
-- TOC entry 5394 (class 0 OID 35464940)
-- Dependencies: 229
-- Data for Name: mission; Type: TABLE DATA; Schema: public; Owner: bnrggdvhxaybke
--

COPY public.mission (id_mission, description, id_station) FROM stdin;
1	Two people lost hiking. Last seen 2 hours ago.	1
\.


--
-- TOC entry 5396 (class 0 OID 35464956)
-- Dependencies: 231
-- Data for Name: mission_task; Type: TABLE DATA; Schema: public; Owner: bnrggdvhxaybke
--

COPY public.mission_task (id_mission_task, id_mission, isfinished, id_transportation_type, id_task_type, id_rescuer) FROM stdin;
1	1	f	3	4	4
2	1	f	1	1	2
3	1	f	1	1	3
\.


--
-- TOC entry 5208 (class 0 OID 35463598)
-- Dependencies: 204
-- Data for Name: spatial_ref_sys; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.spatial_ref_sys (srid, auth_name, auth_srid, srtext, proj4text) FROM stdin;
\.


--
-- TOC entry 5392 (class 0 OID 35464919)
-- Dependencies: 227
-- Data for Name: station; Type: TABLE DATA; Schema: public; Owner: bnrggdvhxaybke
--

COPY public.station (id_station, name, geo_location, id_lead_rescuer) FROM stdin;
2	Mosor	\N	\N
3	Biokovo	\N	\N
4	Papuk	\N	\N
5	Paklenica	\N	\N
1	Medvednica	\N	4
\.


--
-- TOC entry 5384 (class 0 OID 35464870)
-- Dependencies: 219
-- Data for Name: task_type; Type: TABLE DATA; Schema: public; Owner: bnrggdvhxaybke
--

COPY public.task_type (id_task_type, task_name) FROM stdin;
1	Follow specific route
2	Setup temporary station
3	Refresh stock in temporary station
4	Get to a location
\.


--
-- TOC entry 5399 (class 0 OID 35465003)
-- Dependencies: 234
-- Data for Name: transportation_eligibility; Type: TABLE DATA; Schema: public; Owner: bnrggdvhxaybke
--

COPY public.transportation_eligibility (id_rescuer, id_transportation_type) FROM stdin;
2	1
2	2
2	3
4	1
4	2
4	3
3	1
3	2
3	3
\.


--
-- TOC entry 5388 (class 0 OID 35464892)
-- Dependencies: 223
-- Data for Name: transportation_type; Type: TABLE DATA; Schema: public; Owner: bnrggdvhxaybke
--

COPY public.transportation_type (id_transportation_type, transportation_name, visibility_radius) FROM stdin;
1	On foot	10
2	Bicycle	20
3	Car	50
4	Drone	100
5	Helicopter	500
\.


--
-- TOC entry 5386 (class 0 OID 35464881)
-- Dependencies: 221
-- Data for Name: user_role; Type: TABLE DATA; Schema: public; Owner: bnrggdvhxaybke
--

COPY public.user_role (id_role, role) FROM stdin;
1	Administrator
2	Rescuer
3	Dispatcher
\.


--
-- TOC entry 5390 (class 0 OID 35464903)
-- Dependencies: 225
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: bnrggdvhxaybke
--

COPY public.users (id_user, username, password, photo, firstname, lastname, id_role, email, phonenumber, geo_location, approved, id_station_member) FROM stdin;
1	ivanhorvat	12345	\N	Ivan	Horvat	1	ivanhorvat@gmail.com	098123456	\N	t	\N
6	josipivancevic	12345	\N	Josip	Ivančević	2	josipivancevic@mail.com	098123456	\N	f	\N
7	filippetani	12345	\N	Filip	Petani	2	filippetani@mail.com	098123456	\N	f	\N
8	lukakovacevic	12345	\N	Luka	Kovačević	3	lukakovacevic@mail.com	098123456	\N	t	\N
2	fabjanvucina	12345	\N	Fabjan	Vučina	2	fabjanvucina@mail.com	098123456	\N	t	1
3	lukasunara	12345	\N	Luka	Sunara	2	lukasunara@mail.com	098123456	\N	t	1
4	markoknezevic	12345	\N	Marko	Knežević	2	markoknezevic@mail.com	098123456	\N	t	1
\.


--
-- TOC entry 5416 (class 0 OID 0)
-- Dependencies: 232
-- Name: comment_id_comment_seq; Type: SEQUENCE SET; Schema: public; Owner: bnrggdvhxaybke
--

SELECT pg_catalog.setval('public.comment_id_comment_seq', 3, true);


--
-- TOC entry 5417 (class 0 OID 0)
-- Dependencies: 228
-- Name: mission_id_mission_seq; Type: SEQUENCE SET; Schema: public; Owner: bnrggdvhxaybke
--

SELECT pg_catalog.setval('public.mission_id_mission_seq', 1, true);


--
-- TOC entry 5418 (class 0 OID 0)
-- Dependencies: 230
-- Name: mission_task_id_mission_task_seq; Type: SEQUENCE SET; Schema: public; Owner: bnrggdvhxaybke
--

SELECT pg_catalog.setval('public.mission_task_id_mission_task_seq', 3, true);


--
-- TOC entry 5419 (class 0 OID 0)
-- Dependencies: 226
-- Name: station_id_station_seq; Type: SEQUENCE SET; Schema: public; Owner: bnrggdvhxaybke
--

SELECT pg_catalog.setval('public.station_id_station_seq', 5, true);


--
-- TOC entry 5420 (class 0 OID 0)
-- Dependencies: 218
-- Name: task_type_id_task_type_seq; Type: SEQUENCE SET; Schema: public; Owner: bnrggdvhxaybke
--

SELECT pg_catalog.setval('public.task_type_id_task_type_seq', 4, true);


--
-- TOC entry 5421 (class 0 OID 0)
-- Dependencies: 222
-- Name: transportation_type_id_transportation_type_seq; Type: SEQUENCE SET; Schema: public; Owner: bnrggdvhxaybke
--

SELECT pg_catalog.setval('public.transportation_type_id_transportation_type_seq', 5, true);


--
-- TOC entry 5422 (class 0 OID 0)
-- Dependencies: 220
-- Name: user_role_id_role_seq; Type: SEQUENCE SET; Schema: public; Owner: bnrggdvhxaybke
--

SELECT pg_catalog.setval('public.user_role_id_role_seq', 3, true);


--
-- TOC entry 5423 (class 0 OID 0)
-- Dependencies: 224
-- Name: users_id_user_seq; Type: SEQUENCE SET; Schema: public; Owner: bnrggdvhxaybke
--

SELECT pg_catalog.setval('public.users_id_user_seq', 8, true);


--
-- TOC entry 5235 (class 2606 OID 35464992)
-- Name: comment comment_pkey; Type: CONSTRAINT; Schema: public; Owner: bnrggdvhxaybke
--

ALTER TABLE ONLY public.comment
    ADD CONSTRAINT comment_pkey PRIMARY KEY (id_comment);


--
-- TOC entry 5231 (class 2606 OID 35464948)
-- Name: mission mission_pkey; Type: CONSTRAINT; Schema: public; Owner: bnrggdvhxaybke
--

ALTER TABLE ONLY public.mission
    ADD CONSTRAINT mission_pkey PRIMARY KEY (id_mission);


--
-- TOC entry 5233 (class 2606 OID 35464961)
-- Name: mission_task mission_task_pkey; Type: CONSTRAINT; Schema: public; Owner: bnrggdvhxaybke
--

ALTER TABLE ONLY public.mission_task
    ADD CONSTRAINT mission_task_pkey PRIMARY KEY (id_mission_task, id_mission);


--
-- TOC entry 5229 (class 2606 OID 35464927)
-- Name: station station_pkey; Type: CONSTRAINT; Schema: public; Owner: bnrggdvhxaybke
--

ALTER TABLE ONLY public.station
    ADD CONSTRAINT station_pkey PRIMARY KEY (id_station);


--
-- TOC entry 5221 (class 2606 OID 35464878)
-- Name: task_type task_type_pkey; Type: CONSTRAINT; Schema: public; Owner: bnrggdvhxaybke
--

ALTER TABLE ONLY public.task_type
    ADD CONSTRAINT task_type_pkey PRIMARY KEY (id_task_type);


--
-- TOC entry 5237 (class 2606 OID 35465007)
-- Name: transportation_eligibility transportation_eligibility_pkey; Type: CONSTRAINT; Schema: public; Owner: bnrggdvhxaybke
--

ALTER TABLE ONLY public.transportation_eligibility
    ADD CONSTRAINT transportation_eligibility_pkey PRIMARY KEY (id_rescuer, id_transportation_type);


--
-- TOC entry 5225 (class 2606 OID 35464900)
-- Name: transportation_type transportation_type_pkey; Type: CONSTRAINT; Schema: public; Owner: bnrggdvhxaybke
--

ALTER TABLE ONLY public.transportation_type
    ADD CONSTRAINT transportation_type_pkey PRIMARY KEY (id_transportation_type);


--
-- TOC entry 5223 (class 2606 OID 35464889)
-- Name: user_role user_role_pkey; Type: CONSTRAINT; Schema: public; Owner: bnrggdvhxaybke
--

ALTER TABLE ONLY public.user_role
    ADD CONSTRAINT user_role_pkey PRIMARY KEY (id_role);


--
-- TOC entry 5227 (class 2606 OID 35464911)
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: bnrggdvhxaybke
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id_user);


--
-- TOC entry 5246 (class 2606 OID 35464993)
-- Name: comment comment_id_mission_fkey; Type: FK CONSTRAINT; Schema: public; Owner: bnrggdvhxaybke
--

ALTER TABLE ONLY public.comment
    ADD CONSTRAINT comment_id_mission_fkey FOREIGN KEY (id_mission) REFERENCES public.mission(id_mission) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 5247 (class 2606 OID 35464998)
-- Name: comment comment_id_rescuer_fkey; Type: FK CONSTRAINT; Schema: public; Owner: bnrggdvhxaybke
--

ALTER TABLE ONLY public.comment
    ADD CONSTRAINT comment_id_rescuer_fkey FOREIGN KEY (id_rescuer) REFERENCES public.users(id_user) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 5241 (class 2606 OID 35464949)
-- Name: mission mission_id_station_fkey; Type: FK CONSTRAINT; Schema: public; Owner: bnrggdvhxaybke
--

ALTER TABLE ONLY public.mission
    ADD CONSTRAINT mission_id_station_fkey FOREIGN KEY (id_station) REFERENCES public.station(id_station) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- TOC entry 5242 (class 2606 OID 35464962)
-- Name: mission_task mission_task_id_mission_fkey; Type: FK CONSTRAINT; Schema: public; Owner: bnrggdvhxaybke
--

ALTER TABLE ONLY public.mission_task
    ADD CONSTRAINT mission_task_id_mission_fkey FOREIGN KEY (id_mission) REFERENCES public.mission(id_mission) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 5245 (class 2606 OID 35464977)
-- Name: mission_task mission_task_id_rescuer_fkey; Type: FK CONSTRAINT; Schema: public; Owner: bnrggdvhxaybke
--

ALTER TABLE ONLY public.mission_task
    ADD CONSTRAINT mission_task_id_rescuer_fkey FOREIGN KEY (id_rescuer) REFERENCES public.users(id_user) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- TOC entry 5244 (class 2606 OID 35464972)
-- Name: mission_task mission_task_id_task_type_fkey; Type: FK CONSTRAINT; Schema: public; Owner: bnrggdvhxaybke
--

ALTER TABLE ONLY public.mission_task
    ADD CONSTRAINT mission_task_id_task_type_fkey FOREIGN KEY (id_task_type) REFERENCES public.task_type(id_task_type) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- TOC entry 5243 (class 2606 OID 35464967)
-- Name: mission_task mission_task_id_transportation_type_fkey; Type: FK CONSTRAINT; Schema: public; Owner: bnrggdvhxaybke
--

ALTER TABLE ONLY public.mission_task
    ADD CONSTRAINT mission_task_id_transportation_type_fkey FOREIGN KEY (id_transportation_type) REFERENCES public.transportation_type(id_transportation_type) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- TOC entry 5240 (class 2606 OID 35464928)
-- Name: station station_id_lead_rescuer_fkey; Type: FK CONSTRAINT; Schema: public; Owner: bnrggdvhxaybke
--

ALTER TABLE ONLY public.station
    ADD CONSTRAINT station_id_lead_rescuer_fkey FOREIGN KEY (id_lead_rescuer) REFERENCES public.users(id_user) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- TOC entry 5248 (class 2606 OID 35465008)
-- Name: transportation_eligibility transportation_eligibility_id_rescuer_fkey; Type: FK CONSTRAINT; Schema: public; Owner: bnrggdvhxaybke
--

ALTER TABLE ONLY public.transportation_eligibility
    ADD CONSTRAINT transportation_eligibility_id_rescuer_fkey FOREIGN KEY (id_rescuer) REFERENCES public.users(id_user) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 5249 (class 2606 OID 35465013)
-- Name: transportation_eligibility transportation_eligibility_id_transportation_type_fkey; Type: FK CONSTRAINT; Schema: public; Owner: bnrggdvhxaybke
--

ALTER TABLE ONLY public.transportation_eligibility
    ADD CONSTRAINT transportation_eligibility_id_transportation_type_fkey FOREIGN KEY (id_transportation_type) REFERENCES public.transportation_type(id_transportation_type) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 5238 (class 2606 OID 35464912)
-- Name: users users_id_role_fkey; Type: FK CONSTRAINT; Schema: public; Owner: bnrggdvhxaybke
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_id_role_fkey FOREIGN KEY (id_role) REFERENCES public.user_role(id_role) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 5239 (class 2606 OID 35464933)
-- Name: users usersfkstation; Type: FK CONSTRAINT; Schema: public; Owner: bnrggdvhxaybke
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT usersfkstation FOREIGN KEY (id_station_member) REFERENCES public.station(id_station) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- TOC entry 5405 (class 0 OID 0)
-- Dependencies: 4
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: bnrggdvhxaybke
--

REVOKE ALL ON SCHEMA public FROM postgres;
REVOKE ALL ON SCHEMA public FROM PUBLIC;
GRANT ALL ON SCHEMA public TO bnrggdvhxaybke;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- TOC entry 5406 (class 0 OID 0)
-- Dependencies: 1982
-- Name: LANGUAGE plpgsql; Type: ACL; Schema: -; Owner: postgres
--

GRANT ALL ON LANGUAGE plpgsql TO bnrggdvhxaybke;


-- Completed on 2021-01-14 02:50:27

--
-- PostgreSQL database dump complete
--

