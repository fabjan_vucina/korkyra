import React, { useState } from "react";
import PageRouter from "./routers/PageRouter";
import UserContext from "./contexts/UserContext";
import "./App.css";

const App = () => {
  const isAuthenticated = localStorage.getItem("isAuthenticated");
  const displayName = localStorage.getItem("displayName");
  const id_user = localStorage.getItem("id_user");
  const role = localStorage.getItem("role");
  const isLeadRescuer = localStorage.getItem("isLeadRescuer");
  const id_station_member = localStorage.getItem("id_station_member");

  const userHook = useState({
    isAuthenticated: isAuthenticated === "true" ? true : false,
    displayName: isAuthenticated === "true" ? displayName : "",
    id_user: isAuthenticated === "true" ? id_user : "",
    role: isAuthenticated === "true" ? role : "",
    isLeadRescuer: isLeadRescuer === "true" ? true : false,
    id_station_member: isAuthenticated === "true" ? id_station_member : "",
  });

  return (
    <UserContext.Provider value={userHook}>
      <PageRouter />
    </UserContext.Provider>
  );
};

export default App;
