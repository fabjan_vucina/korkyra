import React from "react";
import { Spin } from "antd";
import StationComponent from "../../components/StationComponent/StationComponent";

function StationsListContainer({ fetchedStations, loadingStations }) {
  return (
    <div>
      {loadingStations ? (
        <Spin size="large" />
      ) : (
        fetchedStations.map((station) => (
          <StationComponent
            key={station.id_station}
            id_station={station.id_station}
            name={station.name}
            leadRescuer={
              station.firstname !== null
                ? station.firstname + " " + station.lastname
                : "Nobody"
            }
          />
        ))
      )}
    </div>
  );
}

export default StationsListContainer;
