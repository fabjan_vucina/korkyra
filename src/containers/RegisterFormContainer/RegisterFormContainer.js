import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { Form, Input, Button, Radio } from "antd";
import "./RegisterFormContainer.css";
import AvatarComponent from "../../components/AvatarComponent/AvatarComponent";

const handleRegistrationRequest = async (
  firstName,
  lastName,
  photo,
  roleID,
  phoneNumber,
  email,
  username,
  password,
  history
) => {
  const requestOptions = {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      firstName: firstName,
      lastName: lastName,
      photo: photo,
      id_role: roleID,
      phoneNumber: phoneNumber,
      email: email,
      username: username,
      password: password,
    }),
  };

  fetch("/api/registration", requestOptions)
    .then(async (response) => {
      const data = await response.json();

      if (!response.ok) {
        const error = (data && data.message) || response.status;
        alert(error);
        return Promise.reject(error);
      }

      alert(data.message);
      history.push("/");
    })

    .catch((error) => {
      console.error("There was an error!", error);
    });
};

const RegisterForm = () => {
  let history = useHistory();

  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [photo, setPhoto] = useState(null);
  const [email, setEmail] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");
  const [roleID, setRoleID] = useState("");
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  return (
    <div className="register-form">
      <Form
        onFinish={async () =>
          await handleRegistrationRequest(
            firstName,
            lastName,
            photo,
            roleID,
            phoneNumber,
            email,
            username,
            password,
            history
          )
        }
        labelCol={{
          span: 5,
        }}
        wrapperCol={{
          span: 17,
        }}
        layout="horizontal"
      >
        <Form.Item
          label="First name::"
          name="firstName"
          rules={[{ required: true, message: "Please your first name!" }]}
        >
          <Input
            type="text"
            value={firstName}
            onChange={(e) => {
              setFirstName(e.target.value.toString().trim());
            }}
          />
        </Form.Item>

        <Form.Item
          label="Last name::"
          name="lastName"
          rules={[{ required: true, message: "Please enter your last name!" }]}
        >
          <Input
            type="text"
            value={lastName}
            onChange={(e) => {
              setLastName(e.target.value.toString().trim());
            }}
          />
        </Form.Item>

        <Form.Item
          id="photograph-upload-component"
          label="Photograph::"
          name="photo"
        >
          <AvatarComponent setValue={setPhoto} />
        </Form.Item>

        <Form.Item
          label="Role::"
          name="role"
          rules={[{ required: true, message: "Please select role!" }]}
        >
          <Radio.Group
            type="Radio"
            value={roleID}
            onChange={(e) => {
              setRoleID(e.target.value);
            }}
          >
            <Radio value={2}>Rescuer</Radio>
            <Radio value={3}>Dispatcher</Radio>
          </Radio.Group>
        </Form.Item>

        <Form.Item
          label="Cellphone::"
          name="phoneNumber"
          rules={[{ required: true, message: "Please enter phone number!" }]}
        >
          <Input
            type="tel"
            value={phoneNumber}
            onChange={(e) => {
              setPhoneNumber(e.target.value.toString().trim());
            }}
          />
        </Form.Item>

        <Form.Item
          label="Email::"
          name="email"
          rules={[{ required: true, message: "Please enter email!" }]}
        >
          <Input
            type="email"
            value={email}
            onChange={(e) => {
              setEmail(e.target.value.toString().trim());
            }}
          />
        </Form.Item>

        <Form.Item
          label="Username::"
          name="username"
          rules={[{ required: true, message: "Please enter username!" }]}
        >
          <Input
            type="text"
            value={username}
            onChange={(e) => {
              setUsername(e.target.value.toString().trim());
            }}
          />
        </Form.Item>

        <Form.Item
          label="Password::"
          name="password"
          rules={[{ required: true, message: "Please enter password!" }]}
        >
          <Input.Password
            type="password"
            value={password}
            onChange={(e) => setPassword(e.target.value.toString())}
            autoComplete="current-password"
          />
        </Form.Item>

        <Button
          className="register-submit-btn"
          type="primary"
          htmlType="submit"
        >
          Register
        </Button>
      </Form>
    </div>
  );
};

export default RegisterForm;
