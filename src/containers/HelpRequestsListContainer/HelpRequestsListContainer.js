import React from "react";
import { Spin } from "antd";
import HelpRequestComponent from "../../components/HelpRequestComponent/HelpRequestComponent";

function HelpRequestsListContainer({
  fetchedHelpRequests,
  loadingHelpRequests,
}) {
  return (
    <div>
      {loadingHelpRequests ? (
        <Spin size="large" />
      ) : (
        fetchedHelpRequests.map((helpRequest) => (
          <HelpRequestComponent
            key={helpRequest.id_mission_task}
            missionTaskID={helpRequest.id_mission_task}
            transportationName={helpRequest.transportation_name}
            taskName={helpRequest.task_name}
          />
        ))
      )}
    </div>
  );
}

export default HelpRequestsListContainer;
