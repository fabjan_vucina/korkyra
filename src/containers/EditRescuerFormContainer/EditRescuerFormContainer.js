import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { Form, Input, Button, Radio, Select } from "antd"; //eslint-disable-line
import { HomeOutlined } from "@ant-design/icons";
import "./EditUserFormContainer.css";

const handleRescuerInformationChange = async (
  selectedUserID,
  phoneNumber,
  email,
  stationID,
  history
) => {
  const requestOptions = {
    method: "PUT",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      id_user: selectedUserID,
      phoneNumber: phoneNumber,
      email: email,
      id_station_member: stationID,
    }),
  };

  fetch("/api/changeRescuerInfo", requestOptions)
    .then(async (response) => {
      const data = await response.json();

      if (!response.ok) {
        const error = (data && data.message) || response.status;
        alert(error);
        return Promise.reject(error);
      }

      alert(data.message);
      history.push("/");
    })

    .catch((error) => {
      console.error("There was an error!", error);
    });
};

const EditRescuerFormContainer = ({ selectedUser, selectedUserID }) => {
  let history = useHistory();

  const [email, setEmail] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");
  const [stationID, setStationID] = useState("");

  const [fetchedStations, setFetchedStations] = useState([]);
  const [loadingStations, setLoadingStations] = useState(true);

  useEffect(() => {
    fetch("/api/stations")
      .then(async (response) => {
        const data = await response.json();

        if (!response.ok) {
          const error = (data && data.message) || response.status;
          alert(error);
          return Promise.reject(error);
        }

        setFetchedStations(data);
        setLoadingStations(false);
      })
      .catch((error) => {
        console.error("There was an error!", error);
      });
  }, []);

  return (
    <div className="register-form">
      <div className="selectedUser">{selectedUser}</div>

      <Form
        onFinish={async () => {
          await handleRescuerInformationChange(
            selectedUserID,
            phoneNumber,
            email,
            stationID,
            history
          );
        }}
        labelCol={{
          span: 5,
        }}
        wrapperCol={{
          span: 17,
        }}
        layout="horizontal"
      >
        <Form.Item
          label="Cellphone::"
          name="phoneNumber"
          rules={[{ required: true, message: "Please enter phone number!" }]}
        >
          <Input
            type="tel"
            value={phoneNumber}
            onChange={(e) => {
              setPhoneNumber(e.target.value.toString().trim());
            }}
          />
        </Form.Item>

        <Form.Item
          label="Email::"
          name="email"
          rules={[{ required: true, message: "Please enter email!" }]}
        >
          <Input
            type="email"
            value={email}
            onChange={(e) => {
              setEmail(e.target.value.toString().trim());
            }}
          />
        </Form.Item>

        <Form.Item
          label="Select new station::"
          name="station"
          className="new-station-select"
          rules={[{ required: true, message: "Please select station!" }]}
        >
          <Select
            // defaultValue={currentStationName}
            suffixIcon={<HomeOutlined />}
            onSelect={(e) => {
              setStationID(e);
            }}
            className="select-station-info"
            loading={loadingStations}
          >
            <Select.OptGroup label="Stations">
              {fetchedStations.map((station) => (
                <Select.Option
                  key={station.id_station}
                  value={station.id_station}
                >
                  {station.name}
                </Select.Option>
              ))}
            </Select.OptGroup>
          </Select>
        </Form.Item>

        <Button
          className="register-submit-btn"
          type="primary"
          htmlType="submit"
        >
          Confirm
        </Button>
      </Form>
    </div>
  );
};

export default EditRescuerFormContainer;
