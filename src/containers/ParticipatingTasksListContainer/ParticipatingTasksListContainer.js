import React from "react";
import { Spin } from "antd";
import ParticipatingTaskComponent from "../../components/ParticipatingTaskComponent/ParticipatingTaskComponent";

function ParticipatingTasksListContainer({
  fetchedParticipatingTasks,
  loadingParticipatingTasks,
}) {
  return (
    <div>
      {loadingParticipatingTasks ? (
        <Spin size="large" />
      ) : (
        fetchedParticipatingTasks.map((task) => (
          <ParticipatingTaskComponent
            key={task.id_mission_task}
            missionID={task.id_mission}
            transportationName={task.transportation_name}
            taskName={task.task_name}
          />
        ))
      )}
    </div>
  );
}

export default ParticipatingTasksListContainer;
