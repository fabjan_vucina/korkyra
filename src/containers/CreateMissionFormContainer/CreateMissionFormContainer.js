import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { Form, Input, Button, Select } from "antd";
import { HomeOutlined } from "@ant-design/icons";
import "./CreateMissionFormContainer.css";

const handleMissionCreation = async (description, stationID, history) => {
  const requestOptions = {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      description: description,
      id_station: stationID,
    }),
  };

  fetch("/api/createMission", requestOptions)
    .then(async (response) => {
      const data = await response.json();

      if (!response.ok) {
        const error = (data && data.message) || response.status;
        alert(error);
        return Promise.reject(error);
      }

      alert(data.message);
      history.push("/");
    })

    .catch((error) => {
      console.error("There was an error!", error);
    });
};

const CreateMissionFormContainer = () => {
  let history = useHistory();

  const [description, setDescription] = useState("");
  const [stationID, setStationID] = useState("");
  const [fetchedStations, setFetchedStations] = useState([]);
  const [loadingStations, setLoadingStations] = useState(true);

  useEffect(() => {
    fetch("/api/stations")
      .then(async (response) => {
        const data = await response.json();

        if (!response.ok) {
          const error = (data && data.message) || response.status;
          alert(error);
          return Promise.reject(error);
        }

        setFetchedStations(data);
        setLoadingStations(false);
      })
      .catch((error) => {
        console.error("There was an error!", error);
      });
  }, []);

  return (
    <div className="create-form">
      <Form
        onFinish={async () =>
          await handleMissionCreation(description, stationID, history)
        }
        labelCol={{
          span: 7,
        }}
        wrapperCol={{
          span: 21,
        }}
        layout="horizontal"
      >
        <Form.Item
          label="Description::"
          name="missionDescription"
          rules={[
            { required: true, message: "Please enter description of mission!" },
            { max: 75, message: "Description can be max 75 characters long!" },
          ]}
        >
          <Input.TextArea
            value={description}
            onChange={(e) => {
              setDescription(e.target.value.toString().trim());
            }}
            placeholder="Enter mission description..."
            autoSize={{ minRows: 3, maxRows: 5 }}
            showCount
            maxLength={75}
          />
        </Form.Item>

        <Form.Item
          label="Select station::"
          name="station"
          className="new-station-select"
          rules={[{ required: true, message: "Please select station!" }]}
        >
          <Select
            // defaultValue={currentStationName}
            suffixIcon={<HomeOutlined />}
            onSelect={(e) => {
              setStationID(e);
            }}
            className="select-station-info"
            loading={loadingStations}
          >
            <Select.OptGroup label="Stations">
              {fetchedStations.map((station) => (
                <Select.Option
                  key={station.id_station}
                  value={station.id_station}
                >
                  {station.name}
                </Select.Option>
              ))}
            </Select.OptGroup>
          </Select>
        </Form.Item>

        <Button
          className="register-submit-btn"
          type="primary"
          htmlType="submit"
        >
          Create
        </Button>
      </Form>
    </div>
  );
};

export default CreateMissionFormContainer;
