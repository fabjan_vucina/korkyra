import React, { useState, useContext } from "react";
import { Link, useHistory } from "react-router-dom";
import { Form, Input, Button } from "antd";
import UserContext from "../../contexts/UserContext";
import "./LoginFormContainer.css";

const handleLogin = async (username, password, history, setUser) => {
  const requestOptions = {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      username: username,
      password: password,
    }),
  };

  fetch("/api/login", requestOptions)
    .then(async (response) => {
      const data = await response.json();

      if (!response.ok) {
        const error = (data && data.message) || response.status;
        alert(error);
        return Promise.reject(error);
      }

      localStorage.setItem("isAuthenticated", true);
      localStorage.setItem("displayName", data.firstName + " " + data.lastName);
      localStorage.setItem("id_user", data.id_user);
      localStorage.setItem("role", data.role);
      localStorage.setItem("isLeadRescuer", data.isLeadRescuer);
      localStorage.setItem("id_station_member", data.id_station_member);

      setUser({
        isAuthenticated: true,
        displayName: data.firstName + " " + data.lastName,
        id_user: data.id_user,
        role: data.role,
        isLeadRescuer: data.isLeadRescuer,
        id_station_member: data.id_station_member,
      });

      if (data.role === "Rescuer") {
        if (data.isLeadRescuer === true) {
          history.push("/leadRescuer");
        } else {
          history.push("/rescuer");
        }
      }
      if (data.role === "Dispatcher") {
        history.push("/dispatcher");
      }
      if (data.role === "Administrator") {
        history.push("/admin");
      }
    })

    .catch((error) => {
      console.error("There was an error!", error);
    });
};

const LoginFormContainer = () => {
  let history = useHistory();
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [user, setUser] = useContext(UserContext); //eslint-disable-line

  return (
    <div className="login-form">
      <Form
        onFinish={async () => {
          await handleLogin(username, password, history, setUser);
        }}
        labelCol={{
          span: 5,
        }}
        wrapperCol={{
          span: 17,
        }}
        layout="horizontal"
      >
        <Form.Item
          label="Username::"
          name="username"
          rules={[{ required: true, message: "Please enter username!" }]}
        >
          <Input
            type="username"
            value={username}
            onChange={(e) => {
              setUsername(e.target.value.toString().trim());
            }}
            autoComplete="off"
          />
        </Form.Item>

        <Form.Item
          label="Password::"
          name="password"
          rules={[{ required: true, message: "Please enter password!" }]}
        >
          <Input.Password
            type="password"
            value={password}
            onChange={(e) => setPassword(e.target.value.toString())}
            autoComplete="current-password"
          />
        </Form.Item>

        <Button type="primary" htmlType="submit">
          Log in
        </Button>
        <div className="registerLink">
          <Link to="/register" className="linkWhite registerLink">
            Don{"'"}t have an account? Register here!
          </Link>
        </div>
      </Form>
    </div>
  );
};

export default LoginFormContainer;
