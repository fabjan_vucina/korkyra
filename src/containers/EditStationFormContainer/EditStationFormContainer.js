import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { Form, Input, Button, Radio, Select } from "antd"; //eslint-disable-line
import { UserOutlined } from "@ant-design/icons";
import "./EditStationFormContainer.css";
import "antd/es/select/style/css";

const handleStationInformationChange = async (
  selectedStationID,
  leadRescuerID,
  history
) => {
  const requestOptions = {
    method: "PUT",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      id_station: selectedStationID,
      id_lead_rescuer: leadRescuerID,
    }),
  };

  fetch("/api/changeStationInfo", requestOptions)
    .then(async (response) => {
      const data = await response.json();

      if (!response.ok) {
        const error = (data && data.message) || response.status;
        alert(error);
        return Promise.reject(error);
      }

      alert(data.message);
      history.push("/");
    })

    .catch((error) => {
      console.error("There was an error!", error);
    });
};

const EditStationFormContainer = ({ selectedStation, selectedStationID }) => {
  let history = useHistory();

  const [leadRescuerID, setLeadRescuerID] = useState("");

  const [fetchedRescuers, setFetchedRescuers] = useState([]);
  const [loadingRescuers, setLoadingRescuers] = useState(true);

  useEffect(() => {
    fetch("/api/stationRescuers/" + selectedStationID)
      .then(async (response) => {
        const data = await response.json();

        if (!response.ok) {
          const error = (data && data.message) || response.status;
          alert(error);
          return Promise.reject(error);
        }

        setFetchedRescuers(data);
        setLoadingRescuers(false);
      })
      .catch((error) => {
        console.error("There was an error!", error);
      });
  }, [selectedStationID]);

  return (
    <div className="register-form">
      <div className="selectedStation">{selectedStation}</div>

      <Form
        onFinish={async () =>
          await handleStationInformationChange(
            selectedStationID,
            leadRescuerID,
            history
          )
        }
        labelCol={{
          span: 7,
        }}
        wrapperCol={{
          span: 20,
        }}
        layout="horizontal"
      >
        <Form.Item
          label="Select new lead rescuer::"
          name="lead_rescuer"
          className="new-lead-rescuer-select"
          rules={[{ required: true, message: "Please select lead rescuer!" }]}
        >
          <Select
            // defaultValue={leadRescuerName}
            suffixIcon={<UserOutlined />}
            onSelect={(e) => {
              setLeadRescuerID(e);
            }}
            id="select-row"
            loading={loadingRescuers}
          >
            <Select.OptGroup label="Rescuers">
              {fetchedRescuers.map((rescuer) => (
                <Select.Option key={rescuer.id_user} value={rescuer.id_user}>
                  {rescuer.firstname} {rescuer.lastname}
                </Select.Option>
              ))}
            </Select.OptGroup>
          </Select>
        </Form.Item>
        <Button
          className="register-submit-btn"
          type="primary"
          htmlType="submit"
        >
          Confirm
        </Button>
      </Form>
    </div>
  );
};

export default EditStationFormContainer;
