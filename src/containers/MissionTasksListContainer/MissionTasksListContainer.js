import React from "react";
import { Spin } from "antd";
import MissionTaskComponent from "../../components/MissionTaskComponent/MissionTaskComponent";

function MissionTasksListContainer({
  fetchedMissionTasks,
  loadingMissionTasks,
}) {
  return (
    <div>
      {loadingMissionTasks ? (
        <Spin size="large" />
      ) : (
        fetchedMissionTasks.map((missionTask) => (
          <MissionTaskComponent
            key={missionTask.id_mission_task}
            missionTaskID={missionTask.id_mission_task}
            taskName={missionTask.task_name}
            transportationName={missionTask.transportation_name}
            rescuer={
              missionTask.firstname !== null
                ? missionTask.firstname + " " + missionTask.lastname
                : "Nobody"
            }
            isFinished={missionTask.isfinished}
          />
        ))
      )}
    </div>
  );
}

export default MissionTasksListContainer;
