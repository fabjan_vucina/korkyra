import React from "react";
import { Spin } from "antd";
import MissionInteractCommentComponent from "../../components/MissionInteractCommentComponent/MissionInteractCommentComponent";

function MissionInteractCommentsContainer({
    fetchedComments,
    loadingComments,
}) {
    return (
        <div>
            {loadingComments ? (
                <Spin size="large" />
            ) : (
                    fetchedComments.map((comment) => (
                        <MissionInteractCommentComponent
                            key={comment.id_comment}
                            comment={comment.comment}
                            rescuerName={comment.firstname + " " + comment.lastname}
                        />
                    ))
                )}
        </div>
    );
}

export default MissionInteractCommentsContainer;
