import React from "react";
import { Spin } from "antd";
import UserComponent from "../../components/UserComponent/UserComponent";

function UsersListContainer({ fetchedUsers, loadingUsers }) {
  return (
    <div>
      {loadingUsers ? (
        <Spin size="large" />
      ) : (
        fetchedUsers.map((user) => (
          <UserComponent
            key={user.id_user}
            id_user={user.id_user}
            firstName={user.firstname} //we are accessing attributes of a table entry, thats why its all lowercase
            lastName={user.lastname}
            role={user.role}
            email={user.email}
            phone_number={user.phonenumber}
          />
        ))
      )}
    </div>
  );
}

export default UsersListContainer;
