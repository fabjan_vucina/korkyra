import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { Form, Input, Button, Radio, Select } from "antd"; //eslint-disable-line
import "../EditRescuerFormContainer/EditUserFormContainer.css";

const handleRescuerInformationChange = async (
  selectedUserID,
  phoneNumber,
  email,
  history
) => {
  const requestOptions = {
    method: "PUT",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      id_user: selectedUserID,
      phoneNumber: phoneNumber,
      email: email,
    }),
  };

  fetch("/api/changeDispatcherInfo", requestOptions)
    .then(async (response) => {
      const data = await response.json();

      if (!response.ok) {
        const error = (data && data.message) || response.status;
        alert(error);
        return Promise.reject(error);
      }

      alert(data.message);
      history.push("/");
    })

    .catch((error) => {
      console.error("There was an error!", error);
    });
};

const EditDispatcherFormContainer = ({ selectedUser, selectedUserID }) => {
  let history = useHistory();

  const [email, setEmail] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");

  return (
    <div className="register-form">
      <div className="selectedUser">{selectedUser}</div>

      <Form
        onFinish={async () => {
          await handleRescuerInformationChange(
            selectedUserID,
            phoneNumber,
            email,
            history
          );
        }}
        labelCol={{
          span: 5,
        }}
        wrapperCol={{
          span: 17,
        }}
        layout="horizontal"
      >
        <Form.Item
          label="Cellphone::"
          name="phoneNumber"
          rules={[{ required: true, message: "Please enter phone number!" }]}
        >
          <Input
            type="tel"
            value={phoneNumber}
            onChange={(e) => {
              setPhoneNumber(e.target.value.toString().trim());
            }}
          />
        </Form.Item>

        <Form.Item
          label="Email::"
          name="email"
          rules={[{ required: true, message: "Please enter email!" }]}
        >
          <Input
            type="email"
            value={email}
            onChange={(e) => {
              setEmail(e.target.value.toString().trim());
            }}
          />
        </Form.Item>

        <Button
          className="register-submit-btn"
          type="primary"
          htmlType="submit"
        >
          Confirm
        </Button>
      </Form>
    </div>
  );
};

export default EditDispatcherFormContainer;
