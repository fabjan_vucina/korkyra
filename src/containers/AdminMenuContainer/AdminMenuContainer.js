import React from "react";
import { Link } from "react-router-dom";
import { Divider } from "antd";
import "./AdminMenuContainer.css";

const AdminMenuContainer = () => {
  return (
    <div className="admin-menu-container">
      <Link to="/registrationRequests" className="linkWhite">
        <div className="admin-menu-option">Display registration requests</div>
      </Link>
      <Divider />
      <Link to="/approvedUsers" className="linkWhite">
        <div className="admin-menu-option">Display approved users</div>
      </Link>
      <Divider />
      <Link to="/rescueStations" className="linkWhite">
        <div className="admin-menu-option">Display rescue stations</div>
      </Link>
      <Divider />
      <Link to="/createStation" className="linkWhite">
        <div className="admin-menu-option">Add new station</div>
      </Link>
    </div>
  );
};

export default AdminMenuContainer;
