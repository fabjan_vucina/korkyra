import React from "react";
import { Link } from "react-router-dom";
import { Divider } from "antd";
import "./RescuerMenuContainer.css";

const RescuerMenuContainer = () => {
  return (
    <div className="admin-menu-container">
      <Link to="/helpRequests" className="linkWhite">
        <div className="rescuer-menu-option">Display help requests</div>
      </Link>
      <Divider />
      <Link to="/participatingMissionTasks" className="linkWhite">
        <div className="rescuer-menu-option">
          Display participating mission tasks
        </div>
      </Link>
    </div>
  );
};

export default RescuerMenuContainer;
