import React, { useState, useEffect, useContext } from "react";
import { Form, Input, Button } from "antd";
import UserContext from "../../contexts/UserContext";
import "./MissionInteractContainer.css";
import MissionInteractCommentsContainer from "../MissionInteractCommentsContainer/MissionInteractCommentsContainer";

const handleNewComment = async (userID, newComment, participatingMissionID) => {
  const requestOptions = {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      id_user: userID,
      comment: newComment,
      id_mission: participatingMissionID,
    }),
  };

  fetch("/api/leaveComment", requestOptions)
    .then(async (response) => {
      const data = await response.json();

      if (!response.ok) {
        const error = (data && data.message) || response.status;
        alert(error);
        return Promise.reject(error);
      }

      alert(data.message);
      window.location.reload();
    })

    .catch((error) => {
      console.error("There was an error!", error);
    });
};

const MissionInteractContainer = ({ participatingMissionID }) => {
  const [user, setUser] = useContext(UserContext); //eslint-disable-line

  const [newComment, setNewComment] = useState("");
  const [fetchedComments, setFetchedComments] = useState([]); //eslint-disable-line
  const [loadingComments, setLoadingComments] = useState(true); //eslint-disable-line

  useEffect(() => {
    fetch("/api/missionComments/" + participatingMissionID)
      .then(async (response) => {
        const data = await response.json();

        if (!response.ok) {
          const error = (data && data.message) || response.status;
          alert(error);
          return Promise.reject(error);
        }

        setFetchedComments(data);
        setLoadingComments(false);
      })
      .catch((error) => {
        console.error("There was an error!", error);
      });
  }, [participatingMissionID]);

  return (
    <>
      <div className="comment-form">
        <Form
          onFinish={async () =>
            await handleNewComment(
              user.id_user,
              newComment,
              participatingMissionID
            )
          }
          labelCol={{
            span: 7,
          }}
          wrapperCol={{
            span: 21,
          }}
          layout="horizontal"
        >
          <Form.Item
            label="Comment::"
            name="newComment"
            rules={[
              { required: true, message: "Please enter a new comment!" },
              { max: 75, message: "Comment can be max 75 characters long!" },
            ]}
          >
            <Input.TextArea
              value={newComment}
              onChange={(e) => {
                setNewComment(e.target.value.toString().trim());
              }}
              placeholder="Enter new comment...(Type two ' chars for a single quote)"
              autoSize={{ minRows: 2, maxRows: 5 }}
              showCount
              maxLength={75}
            />
          </Form.Item>

          <Button
            className="register-submit-btn"
            type="primary"
            htmlType="submit"
          >
            Comment
          </Button>
        </Form>
      </div>

      <MissionInteractCommentsContainer
        fetchedComments={fetchedComments}
        loadingComments={loadingComments}
      />
    </>
  );
};

export default MissionInteractContainer;
