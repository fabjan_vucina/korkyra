import React from "react";
import "./HeaderContainer.css";
import HeaderLogoComponent from "../../components/HeaderLogoComponent/HeaderLogoComponent.js";
import LoginTabContainer from "../../containers/LoginTabContainer/LoginTabContainer.js";

const HeaderContainer = ({ includeLoginTab, loggedIn, displayName }) => {
  return (
    <div className="header">
      <HeaderLogoComponent />
      <LoginTabContainer
        display={includeLoginTab}
        loggedIn={loggedIn}
        displayName={displayName}
      />
    </div>
  );
};

export default HeaderContainer;
