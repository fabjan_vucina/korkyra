import React from "react";
import { Spin } from "antd";
import RegistrationRequestComponent from "../../components/RegistrationRequestComponent/RegistrationRequestComponent";

function RegistrationRequestsListContainer({
  fetchedRequests,
  loadingRequests,
}) {
  return (
    <div>
      {loadingRequests ? (
        <Spin size="large" />
      ) : (
        fetchedRequests.map((request) => (
          <RegistrationRequestComponent
            key={request.id_user}
            id_user={request.id_user}
            firstName={request.firstname} //we are accessing attributes of a table entry, thats why its all lowercase
            lastName={request.lastname}
            role={request.role}
            email={request.email}
            phone_number={request.phonenumber}
          />
        ))
      )}
    </div>
  );
}

export default RegistrationRequestsListContainer;
