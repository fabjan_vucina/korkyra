import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { Form, Button, Select } from "antd"; //eslint-disable-line
import { CarOutlined } from "@ant-design/icons";
import "./EditStationRescuerFormContainer.css";

const handleTransportationEligibilitiesDeletion = async (selectedRescuerID) => {
  const requestOptionsDeletion = {
    method: "DELETE",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      id_user: selectedRescuerID,
    }),
  };

  fetch("/api/removeTransportationEligibilities", requestOptionsDeletion)
    .then(async (response) => {
      const data = await response.json();

      if (!response.ok) {
        const error = (data && data.message) || response.status;
        alert(error);
        return Promise.reject(error);
      }
    })

    .catch((error) => {
      console.error("There was an error!", error);
    });
};

const handleTransportationEligibilitiesAssignment = async (
  selectedRescuerID,
  transportationTypesIDs,
  history
) => {
  const requestOptionsAssignment = {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      id_user: selectedRescuerID,
      transportation_types: transportationTypesIDs,
    }),
  };

  fetch("/api/assignTransportationTypes", requestOptionsAssignment)
    .then(async (response) => {
      const data = await response.json();

      if (!response.ok) {
        const error = (data && data.message) || response.status;
        alert(error);
        return Promise.reject(error);
      }

      alert(data.message);
      history.push("/");
    })

    .catch((error) => {
      console.error("There was an error!", error);
    });
};

const EditStationRescuerFormContainer = ({
  selectedRescuer,
  selectedRescuerID,
}) => {
  let history = useHistory();

  const [transportationTypesIDs, setTransportationTypesIDs] = useState([]);
  const [fetchedTransportationTypes, setFetchedTransportationTypes] = useState(
    []
  );
  const [loadingTransportationTypes, setLoadingTransportationTypes] = useState(
    true
  );

  useEffect(() => {
    fetch("/api/transportationTypes")
      .then(async (response) => {
        const data = await response.json();

        if (!response.ok) {
          const error = (data && data.message) || response.status;
          alert(error);
          return Promise.reject(error);
        }

        setFetchedTransportationTypes(data);
        setLoadingTransportationTypes(false);
      })
      .catch((error) => {
        console.error("There was an error!", error);
      });
  }, []);

  return (
    <div className="edit-station-rescuer-form">
      <h3 className="selectedStationRescuer">{selectedRescuer}</h3>
      <Form
        onFinish={async () => {
          await handleTransportationEligibilitiesDeletion(selectedRescuerID);
          await handleTransportationEligibilitiesAssignment(
            selectedRescuerID,
            transportationTypesIDs,
            history
          );
        }}
        labelCol={{
          span: 7,
        }}
        wrapperCol={{
          span: 22,
        }}
        layout="horizontal"
      >
        <Form.Item
          label="Select transportation type::"
          name="transportationType"
          className="transportationType"
          rules={[
            { required: true, message: "Please select transportation type!" },
          ]}
        >
          <Select
            mode="multiple"
            suffixIcon={<CarOutlined />}
            onChange={(e) => {
              setTransportationTypesIDs(e);
            }}
            id="select-row"
            loading={loadingTransportationTypes}
          >
            <Select.OptGroup label="Transportation types">
              {fetchedTransportationTypes.map((transportation) => (
                <Select.Option
                  key={transportation.id_transportation_type}
                  value={transportation.id_transportation_type}
                >
                  {transportation.transportation_name}
                </Select.Option>
              ))}
            </Select.OptGroup>
          </Select>
        </Form.Item>

        <Button className="create-task-btn" type="primary" htmlType="submit">
          Edit
        </Button>
      </Form>
    </div>
  );
};

export default EditStationRescuerFormContainer;
