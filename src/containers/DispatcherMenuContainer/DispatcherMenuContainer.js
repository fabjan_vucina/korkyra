import React from "react";
import { Link } from "react-router-dom";
import { Divider } from "antd";
import "./DispatcherMenuContainer.css";

const DispatcherMenuContainer = () => {
  return (
    <div className="dispatcher-menu-container">
      <Link to="/createMission" className="linkWhite">
        <div className="dispatcher-menu-option">Create new mission</div>
      </Link>
      <Divider />
      <Link to="/missions" className="linkWhite">
        <div className="dispatcher-menu-option">Display missions</div>
      </Link>
    </div>
  );
};

export default DispatcherMenuContainer;
