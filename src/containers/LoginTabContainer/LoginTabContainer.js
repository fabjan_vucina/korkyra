import React from "react";
import { Link } from "react-router-dom";
import "./LoginTabContainer.css";
import LoggedInDisplayComponent from "../../components/LoggedInDisplayComponent/LoggedInDisplayComponent";
import LoggedOutDisplayComponent from "../../components/LoggedOutDisplayComponent/LoggedOutDisplayComponent";

const LoginTabContainer = ({ display, loggedIn, displayName }) => {
  return (
    <div
      className={display ? "login-tab-container" : "login-tab-container hide"}
    >
      {loggedIn ? (
        <LoggedInDisplayComponent displayName={displayName} />
      ) : (
        <Link to="/login" className="linkWhite">
          <LoggedOutDisplayComponent />
        </Link>
      )}
    </div>
  );
};

export default LoginTabContainer;
