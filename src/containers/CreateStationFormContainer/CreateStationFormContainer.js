import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { Form, Input, Button, Radio, Select } from "antd"; //eslint-disable-line
import "./CreateStationFormContainer.css";

const handleStationCreation = async (name, history) => {
  const requestOptions = {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      name: name,
    }),
  };

  fetch("/api/addStation", requestOptions)
    .then(async (response) => {
      const data = await response.json();

      if (!response.ok) {
        const error = (data && data.message) || response.status;
        alert(error);
        return Promise.reject(error);
      }

      alert(data.message);
      history.push("/");
    })

    .catch((error) => {
      console.error("There was an error!", error);
    });
};

const CreateStationFormContainer = () => {
  let history = useHistory();

  const [name, setName] = useState("");

  return (
    <div className="register-form station-form">
      <Form
        onFinish={async () => await handleStationCreation(name, history)}
        labelCol={{
          span: 5,
        }}
        wrapperCol={{
          span: 17,
        }}
        layout="horizontal"
      >
        <Form.Item
          label="Name::"
          name="stationName"
          rules={[{ required: true, message: "Please enter station name!" }]}
        >
          <Input
            type="tel"
            value={name}
            onChange={(e) => {
              setName(e.target.value.toString().trim());
            }}
          />
        </Form.Item>

        <Button
          className="register-submit-btn"
          type="primary"
          htmlType="submit"
        >
          Create
        </Button>
      </Form>
    </div>
  );
};

export default CreateStationFormContainer;
