import React from "react";
import { Spin } from "antd";
import StationRescuerComponent from "../../components/StationRescuerComponent/StationRescuerComponent";

function StationRescuersListContainer({ fetchedStationRescuers, loadingStationRescuers }) {
  return (
    <div>
      {loadingStationRescuers ? (
        <Spin size="large" />
      ) : (
          fetchedStationRescuers.map((rescuer) => (
            <StationRescuerComponent
              key={rescuer.id_user}
              id_user={rescuer.id_user}
              firstName={rescuer.firstname} //we are accessing attributes of a table entry, thats why its all lowercase
              lastName={rescuer.lastname}
            />
          ))
        )}
    </div>
  );
}

export default StationRescuersListContainer;
