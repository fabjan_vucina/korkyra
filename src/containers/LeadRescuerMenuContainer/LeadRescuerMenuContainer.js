import React from "react";
import { Link } from "react-router-dom";
import { Divider } from "antd";
import "./LeadRescuerMenuContainer.css";

const LeadRescuerMenuContainer = () => {
  return (
    <div className="leadRescuer-menu-container">
      <Link to="/helpRequests" className="linkWhite">
        <div className="leadRescuer-menu-option">Display help requests</div>
      </Link>
      <Divider>
        <Link to="/participatingMissionTasks" className="linkWhite">
          <div className="leadRescuer-menu-option">
            Display participating mission tasks
          </div>
        </Link>
      </Divider>
      <Divider>
        <Link to="/stationRescuers" className="linkWhite">
          <div className="leadRescuer-menu-option">Manage station rescuers</div>
        </Link>
      </Divider>
    </div>
  );
};

export default LeadRescuerMenuContainer;
