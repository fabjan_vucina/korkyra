import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { Form, Button, Select } from "antd";
import { CarOutlined, FundProjectionScreenOutlined } from "@ant-design/icons";
import "./CreateMissionTaskFormContainer.css";

const handleMissionTaskCreation = async (
  missionID,
  transportationTypeID,
  taskTypeID,
  history
) => {
  const requestOptions = {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      id_transportation_type: transportationTypeID,
      id_task_type: taskTypeID,
      id_mission: missionID,
    }),
  };

  fetch("/api/createMissionTask", requestOptions)
    .then(async (response) => {
      const data = await response.json();

      if (!response.ok) {
        const error = (data && data.message) || response.status;
        alert(error);
        return Promise.reject(error);
      }

      alert(data.message);
      history.push("/");
    })

    .catch((error) => {
      console.error("There was an error!", error);
    });
};

const CreateMissionTaskFormContainer = () => {
  let history = useHistory();

  const selectedMissionID = localStorage.getItem("selectedMissionID");

  const [transportationTypeID, setTransportationTypeID] = useState("");
  const [taskTypeID, setTaskTypeID] = useState("");
  const [fetchedTransportationTypes, setFetchedTransportationTypes] = useState(
    []
  );
  const [loadingTransportationTypes, setLoadingTransportationTypes] = useState(
    true
  );
  const [fetchedTaskTypes, setFetchedTaskTypes] = useState([]);
  const [loadingTaskTypes, setLoadingTaskTypes] = useState(true);

  useEffect(() => {
    fetch("/api/transportationTypes")
      .then(async (response) => {
        const data = await response.json();

        if (!response.ok) {
          const error = (data && data.message) || response.status;
          alert(error);
          return Promise.reject(error);
        }

        setFetchedTransportationTypes(data);
        setLoadingTransportationTypes(false);
      })
      .catch((error) => {
        console.error("There was an error!", error);
      });
  }, []);

  useEffect(() => {
    fetch("/api/taskTypes")
      .then(async (response) => {
        const data = await response.json();

        if (!response.ok) {
          const error = (data && data.message) || response.status;
          alert(error);
          return Promise.reject(error);
        }

        setFetchedTaskTypes(data);
        setLoadingTaskTypes(false);
      })
      .catch((error) => {
        console.error("There was an error!", error);
      });
  }, []);

  return (
    <div className="create-task-form">
      <Form
        onFinish={async () =>
          await handleMissionTaskCreation(
            selectedMissionID,
            transportationTypeID,
            taskTypeID,
            history
          )
        }
        labelCol={{
          span: 7,
        }}
        wrapperCol={{
          span: 22,
        }}
        layout="horizontal"
      >
        <Form.Item
          label="Select transportation type::"
          name="transportationType"
          className="transportationType"
          rules={[
            { required: true, message: "Please select transportation type!" },
          ]}
        >
          <Select
            suffixIcon={<CarOutlined />}
            onSelect={(e) => {
              setTransportationTypeID(e);
            }}
            id="select-row"
            loading={loadingTransportationTypes}
          >
            <Select.OptGroup label="Transportation types">
              {fetchedTransportationTypes.map((transportation) => (
                <Select.Option
                  key={transportation.id_transportation_type}
                  value={transportation.id_transportation_type}
                >
                  {transportation.transportation_name}
                </Select.Option>
              ))}
            </Select.OptGroup>
          </Select>
        </Form.Item>

        <Form.Item
          label="Select task type::"
          name="taskType"
          className="task-type-select"
          rules={[{ required: true, message: "Please select a task type!" }]}
        >
          <Select
            suffixIcon={<FundProjectionScreenOutlined />}
            onSelect={(e) => {
              setTaskTypeID(e);
            }}
            loading={loadingTaskTypes}
          >
            <Select.OptGroup label="Task types">
              {fetchedTaskTypes.map((taskType) => (
                <Select.Option
                  key={taskType.id_task_type}
                  value={taskType.id_task_type}
                >
                  {taskType.task_name}
                </Select.Option>
              ))}
            </Select.OptGroup>
          </Select>
        </Form.Item>

        <Button className="create-task-btn" type="primary" htmlType="submit">
          Create
        </Button>
      </Form>
    </div>
  );
};

export default CreateMissionTaskFormContainer;
