import React from "react";
import { Spin } from "antd";
import MissionComponent from "../../components/MissionComponent/MissionComponent";

function MissionsListContainer({ fetchedMissions, loadingMissions }) {
  return (
    <div>
      {loadingMissions ? (
        <Spin size="large" />
      ) : (
        fetchedMissions.map((mission) => (
          <MissionComponent
            key={mission.id_mission}
            id_mission={mission.id_mission}
            description={mission.description}
            stationName={mission.name}
          />
        ))
      )}
    </div>
  );
}

export default MissionsListContainer;