import React from "react";
import { useHistory } from "react-router-dom";
import { Button } from "antd";
import "./StationComponent.css";

const handleStationDeletion = async (id_station) => {
  const requestOptions = {
    method: "DELETE",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      id_station: id_station,
    }),
  };

  fetch("/api/deleteStation", requestOptions)
    .then(async (response) => {
      const data = await response.json();

      if (!response.ok) {
        const error = (data && data.message) || response.status;
        alert(error);
        return Promise.reject(error);
      }

      alert(data.message);
      window.location.reload();
    })

    .catch((error) => {
      console.error("There was an error!", error);
    });
};

const StationComponent = ({ id_station, name, leadRescuer }) => {
  const history = useHistory();

  return (
    <div className="station">
      <span className="station-detail">
        <span className="station-attribute">Station:</span> {name}
      </span>
      <span className="station-detail">
        <span className="station-attribute">Lead rescuer:</span> {leadRescuer}
      </span>
      <Button
        className="edit-btn"
        onClick={async () => {
          localStorage.removeItem("selectedStation");
          localStorage.removeItem("selectedStationID");
          localStorage.setItem("selectedStationID", id_station);
          localStorage.setItem("selectedStation", name);
          history.push("/changeStationInfo");
        }}
      >
        EDIT
      </Button>
      <Button
        className="delete-btn"
        onClick={async () => await handleStationDeletion(id_station)}
      >
        DELETE
      </Button>
    </div>
  );
};

export default StationComponent;
