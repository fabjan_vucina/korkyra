import React, { useContext } from "react";
import { Button } from "antd";
import UserContext from "../../contexts/UserContext";
import "./HelpRequestComponent.css";

const handleHelpRequestAcceptance = async (userID, missionTaskID) => {
  const requestOptions = {
    method: "PUT",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      id_user: userID,
      id_mission_task: missionTaskID,
    }),
  };

  fetch("/api/acceptHelpRequest", requestOptions)
    .then(async (response) => {
      const data = await response.json();
      if (!response.ok) {
        const error = (data && data.message) || response.status;
        alert(error);
        return Promise.reject(error);
      }

      alert(data.message);
      window.location.reload();
    })

    .catch((error) => {
      console.error("There was an error!", error);
    });
};

const UserComponent = ({ missionTaskID, transportationName, taskName }) => {
  const [user, setUser] = useContext(UserContext); //eslint-disable-line

  return (
    <div className="helpRequest">
      <span className="helpRequest-detail">
        <span className="helpRequest-attribute">Transportation type:</span>{" "}
        {transportationName}
      </span>
      <span className="helpRequest-detail">
        <span className="helpRequest-attribute">Task type:</span> {taskName}
      </span>
      <Button
        className="accept-btn"
        onClick={async () => {
          const userID = user.id_user;
          await handleHelpRequestAcceptance(userID, missionTaskID);
        }}
      >
        ACCEPT
      </Button>
    </div>
  );
};

export default UserComponent;
