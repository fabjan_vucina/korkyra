import React from "react";
import { Link } from "react-router-dom";
import { Divider, Spin } from "antd";
import "./MissionMenuComponent.css";

const DispatcherMenuContainer = ({ fetchedInfo, loadingInfo }) => {
  console.log(fetchedInfo);
  return (
    <div>
      {loadingInfo ? (
        <Spin size="large" />
      ) : (
        <div className="dispatcher-div">
          <span className="dispatcher-description">
            <span className="dispatcher-detail">Station:</span>
            {fetchedInfo[0].name}
          </span>
          <span className="dispatcher-description">
            <span className="dispatcher-detail">Description:</span>
            {fetchedInfo[0].description}
          </span>
        </div>
      )}

      <div className="dispatcher-menu-container">
        <Link to="/createMissionTask" className="linkWhite">
          <div className="dispatcher-menu-option">Create new task</div>
        </Link>
        <Divider />
        <Link to="/missionTasks" className="linkWhite">
          <div className="dispatcher-menu-option">Display existing tasks</div>
        </Link>
      </div>
    </div>
  );
};

export default DispatcherMenuContainer;
