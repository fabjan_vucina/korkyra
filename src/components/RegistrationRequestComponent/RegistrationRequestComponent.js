import React from "react";
import { Button } from "antd";
import "./RegistrationRequestComponent.css";

const handleRequestApproval = async (id_user) => {
  const requestOptions = {
    method: "PUT",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      id_user: id_user,
    }),
  };

  fetch("/api/approveRequest", requestOptions)
    .then(async (response) => {
      const data = await response.json();

      if (!response.ok) {
        const error = (data && data.message) || response.status;
        alert(error);
        return Promise.reject(error);
      }

      alert(data.message);
      window.location.reload();
    })

    .catch((error) => {
      console.error("There was an error!", error);
    });
};

const handleRequestRejection = async (id_user) => {
  const requestOptions = {
    method: "DELETE",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      id_user: id_user,
    }),
  };

  fetch("/api/rejectRequest", requestOptions)
    .then(async (response) => {
      const data = await response.json();

      if (!response.ok) {
        const error = (data && data.message) || response.status;
        alert(error);
        return Promise.reject(error);
      }

      alert(data.message);
      window.location.reload();
    })

    .catch((error) => {
      console.error("There was an error!", error);
    });
};

const RegistrationRequestComponent = ({
  id_user,
  firstName,
  lastName,
  role,
  email,
  phone_number,
}) => {
  return (
    <div className="request">
      <span className="request-detail">
        <span className="request-attribute">Name:</span> {firstName} {lastName}
      </span>
      <span className="request-detail">
        <span className="request-attribute">Role:</span> {role}
      </span>
      <span className="request-detail">
        <span className="request-attribute">Email:</span> {email}
      </span>
      <span className="request-detail">
        <span className="request-attribute">Phone number:</span> {phone_number}
      </span>
      <Button
        className="approve-btn"
        onClick={async () => await handleRequestApproval(id_user)}
      >
        APPROVE
      </Button>
      <Button
        className="reject-btn"
        onClick={async () => await handleRequestRejection(id_user)}
      >
        REJECT
      </Button>
    </div>
  );
};

export default RegistrationRequestComponent;
