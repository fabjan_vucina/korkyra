import React from "react";
import { useHistory } from "react-router-dom";
import { Button } from "antd";
import "./MissionComponent.css";

const handleMissionDeletion = async (id_mission) => {
  const requestOptions = {
    method: "DELETE",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      id_mission: id_mission,
    }),
  };

  fetch("/api/deleteMission", requestOptions)
    .then(async (response) => {
      const data = await response.json();

      if (!response.ok) {
        const error = (data && data.message) || response.status;
        alert(error);
        return Promise.reject(error);
      }

      alert(data.message);
      window.location.reload();
    })

    .catch((error) => {
      console.error("There was an error!", error);
    });
};

const MissionComponent = ({ id_mission, description, stationName }) => {
  const history = useHistory();

  return (
    <div className="mission">
      <span className="mission-detail">
        <span className="mission-attribute">Station:</span> {stationName}
      </span>
      <span className="mission-detail mission-description">
        <span className="mission-attribute">Description:</span> {description}
      </span>
      <Button
        className="manage-btn"
        onClick={async () => {
          localStorage.removeItem("selectedMissionID");
          localStorage.setItem("selectedMissionID", id_mission);

          history.push("/manageMission");
        }}
      >
        MANAGE
      </Button>
      <Button
        className="delete-btn"
        onClick={async () => await handleMissionDeletion(id_mission)}
      >
        DELETE
      </Button>
    </div>
  );
};

export default MissionComponent;
