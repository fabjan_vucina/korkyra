import React from "react";
import { useHistory } from "react-router-dom";
import { Button } from "antd";
import "./UserComponent.css";

const handleUserDeletion = async (id_user) => {
  const requestOptions = {
    method: "DELETE",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      id_user: id_user,
    }),
  };

  fetch("/api/deleteUser", requestOptions)
    .then(async (response) => {
      const data = await response.json();

      if (!response.ok) {
        const error = (data && data.message) || response.status;
        alert(error);
        return Promise.reject(error);
      }

      alert(data.message);
      window.location.reload();
    })

    .catch((error) => {
      console.error("There was an error!", error);
    });
};

const UserComponent = ({
  id_user,
  firstName,
  lastName,
  role,
  email,
  phone_number,
}) => {
  const history = useHistory();

  return (
    <div className="user">
      <span className="user-detail">
        <span className="user-attribute">Name:</span> {firstName} {lastName}
      </span>
      <span className="user-detail">
        <span className="user-attribute">Role:</span> {role}
      </span>
      <span className="user-detail">
        <span className="user-attribute">Email:</span> {email}
      </span>
      <span className="user-detail">
        <span className="user-attribute">Phone number:</span> {phone_number}
      </span>
      <Button
        className="edit-btn"
        onClick={async () => {
          localStorage.removeItem("selectedUser");
          localStorage.removeItem("selectedUserID");
          localStorage.removeItem("userRole");
          localStorage.setItem("selectedUserID", id_user);
          localStorage.setItem("selectedUser", firstName + " " + lastName);
          localStorage.setItem("userRole", role);

          if (role === "Rescuer") {
            history.push("/changeRescuerInfo");
          } else {
            history.push("/changeDispatcherInfo");
          }
        }}
      >
        EDIT
      </Button>
      <Button
        className="delete-btn"
        onClick={async () => await handleUserDeletion(id_user)}
      >
        DELETE
      </Button>
    </div>
  );
};

export default UserComponent;
