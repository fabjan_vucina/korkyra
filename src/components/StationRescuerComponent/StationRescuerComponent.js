import React from "react";
import { useHistory } from "react-router-dom";
import { Button } from "antd";
import "./StationRescuerComponent.css";

const StationRescuerComponent = ({ id_user, firstName, lastName }) => {
  const history = useHistory();

  return (
    <div className="rescuer">
      <span className="rescuer-detail">
        <span className="user-attribute">Rescuer:</span> {firstName} {lastName}
      </span>
      <Button
        className="edit-btn bigger-edit-btn"
        onClick={async () => {
          localStorage.removeItem("selectedRescuer");
          localStorage.removeItem("selectedRescuerID");
          localStorage.setItem("selectedRescuerID", id_user);
          localStorage.setItem("selectedRescuer", firstName + " " + lastName);

          history.push("/changeStationRescuerInfo");
        }}
      >
        EDIT
      </Button>
    </div>
  );
};

export default StationRescuerComponent;
