import React from "react";
import "./MissionInteractCommentComponent.css";

const MissionInteractCommentComponent = ({ comment, rescuerName }) => {
    return (
        <div className="comment">
            <span className="comment-detail">
                <span className="comment-attribute">Rescuer:</span> {rescuerName}
            </span>
            <span className="comment-detail">
                <span className="comment-attribute">Comment:</span> {comment}
            </span>
        </div>
    );
};

export default MissionInteractCommentComponent;