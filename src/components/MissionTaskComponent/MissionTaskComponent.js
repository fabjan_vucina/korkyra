import React from "react";
// import { useHistory } from "react-router-dom";
import { Button } from "antd";
import "./MissionTaskComponent.css";

const handleMissionTaskFinishing = async (missionTaskID) => {
  const requestOptions = {
    method: "PUT",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      id_mission_task: missionTaskID,
    }),
  };

  fetch("/api/finishMissionTask", requestOptions)
    .then(async (response) => {
      const data = await response.json();

      if (!response.ok) {
        const error = (data && data.message) || response.status;
        alert(error);
        return Promise.reject(error);
      }

      alert(data.message);
      window.location.reload();
    })

    .catch((error) => {
      console.error("There was an error!", error);
    });
};

const MissionTaskComponent = ({
  missionTaskID,
  transportationName,
  taskName,
  rescuer,
  isFinished,
}) => {
  // const history = useHistory();

  return (
    <div className="missionTask">
      <span className="missionTask-detail">
        <span className="missionTask-attribute">Transportation type:</span>{" "}
        {transportationName}
      </span>
      <span className="missionTask-detail">
        <span className="missionTask-attribute">Task type:</span> {taskName}
      </span>
      <span className="missionTask-detail">
        <span className="missionTask-attribute">Rescuer:</span> {rescuer}
      </span>
      {isFinished ? (
        <></>
      ) : (
        <Button
          className="finish-btn"
          onClick={async () => await handleMissionTaskFinishing(missionTaskID)}
        >
          FINISH
        </Button>
      )}
    </div>
  );
};

export default MissionTaskComponent;
