import React, { useContext } from "react";
import { useHistory } from "react-router-dom";
import UserContext from "../../contexts/UserContext";
import "./LoggedInDisplayComponent.css";

const handleLogout = (setUser, history) => {
  localStorage.clear();

  setUser({
    isAuthenticated: false,
    displayName: "",
    id_user: "",
    role: "",
  });

  history.push("/");
};

const LoggedInDisplayComponent = ({ displayName }) => {
  const history = useHistory();
  const [user, setUser] = useContext(UserContext); //eslint-disable-line

  return (
    <span>
      <span className="displayName">{displayName}</span>
      <span>|</span>
      <span
        className="logout"
        role="button"
        tabIndex={0}
        onClick={() => handleLogout(setUser, history)}
        onKeyDown={() => handleLogout(setUser, history)}
      >
        Logout
      </span>
    </span>
  );
};

export default LoggedInDisplayComponent;
