import React from "react";
import "./WelcomeComponent.css";

const WelcomeComponent = () => {
  return (
    <div className="welcome-image">
      <div className="welcome-message">
        Welcome to the future of <span className="emphasis">rescue.</span>
      </div>
    </div>
  );
};

export default WelcomeComponent;
