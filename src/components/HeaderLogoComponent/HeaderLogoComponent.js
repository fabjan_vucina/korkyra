import React from "react";
import logo from "../../img/hgsstracks_logo.png";
import "./HeaderLogoComponent.css";

const HeaderLogoComponent = () => {
  return <img src={logo} className="logo" alt="logo" id="logo" />;
};

export default HeaderLogoComponent;
