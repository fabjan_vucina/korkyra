import React from "react";
import { useHistory } from "react-router-dom";
import { Button } from "antd";
import "./ParticipatingTaskComponent.css";

const ParticipatingTaskComponent = ({
  missionID,
  transportationName,
  taskName,
}) => {
  const history = useHistory();

  return (
    <div className="task">
      <span className="task-detail">
        <span className="task-attribute">Transportation type:</span>{" "}
        {transportationName}
      </span>
      <span className="task-detail">
        <span className="task-attribute">Task type:</span> {taskName}
      </span>
      <Button
        className="interact-btn"
        onClick={async () => {
          localStorage.removeItem("participatingMissionID");
          localStorage.setItem("participatingMissionID", missionID);
          history.push("/mission");
        }}
      >
        INTERACT
      </Button>
    </div>
  );
};

export default ParticipatingTaskComponent;
