import React, { useContext } from "react";
import HeaderContainer from "../containers/HeaderContainer/HeaderContainer.js";
import EditStationFormContainer from "../containers/EditStationFormContainer/EditStationFormContainer.js";
import UserContext from "../contexts/UserContext";

const EditStationPage = () => {
  const selectedStationID = localStorage.getItem("selectedStationID");
  const selectedStation = localStorage.getItem("selectedStation");

  const [user, setUser] = useContext(UserContext); //eslint-disable-line

  return (
    <div>
      <HeaderContainer
        includeLoginTab={true}
        loggedIn={user.isAuthenticated}
        displayName={user.displayName}
      />
      <EditStationFormContainer
        selectedStation={selectedStation}
        selectedStationID={selectedStationID}
      />
    </div>
  );
};

export default EditStationPage;
