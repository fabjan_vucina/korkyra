import React, { useContext } from "react";
import HeaderContainer from "../containers/HeaderContainer/HeaderContainer.js";
import UserContext from "../contexts/UserContext";
import RescuerMenuContainer from "../containers/RescuerMenuContainer/RescuerMenuContainer";

const RescuerPage = () => {
  const [user, setUser] = useContext(UserContext); //eslint-disable-line

  return (
    <div>
      <HeaderContainer
        includeLoginTab={true}
        loggedIn={user.isAuthenticated}
        displayName={user.displayName}
      />
      <RescuerMenuContainer />
    </div>
  );
};

export default RescuerPage;
