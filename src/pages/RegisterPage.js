import React from "react";
import HeaderContainer from "../containers/HeaderContainer/HeaderContainer.js";
import RegisterFormContainer from "../containers/RegisterFormContainer/RegisterFormContainer.js";

const RegisterPage = () => {
  return (
    <div>
      <HeaderContainer
        includeLoginTab={false}
        loggedIn={false}
        displayName={"John Doe"}
      />
      <RegisterFormContainer />
    </div>
  );
};

export default RegisterPage;
