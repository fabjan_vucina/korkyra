import React, { useContext } from "react";
import HeaderContainer from "../containers/HeaderContainer/HeaderContainer.js";
import CreateMissionTaskFormContainer from "../containers/CreateMissionTaskFormContainer/CreateMissionTaskFormContainer.js";
import UserContext from "../contexts/UserContext";

const CreateMissionTaskPage = () => {
    const [user, setUser] = useContext(UserContext); //eslint-disable-line

    return (
        <div>
            <HeaderContainer
                includeLoginTab={true}
                loggedIn={user.isAuthenticated}
                displayName={user.displayName}
            />
            <CreateMissionTaskFormContainer />
        </div>
    );
};

export default CreateMissionTaskPage;
