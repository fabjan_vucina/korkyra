import React, { useContext } from "react";
import HeaderContainer from "../containers/HeaderContainer/HeaderContainer.js";
import EditRescuerFormContainer from "../containers/EditRescuerFormContainer/EditRescuerFormContainer.js";
import UserContext from "../contexts/UserContext";

const EditRescuerPage = () => {
  const selectedUserID = localStorage.getItem("selectedUserID");
  const selectedUser = localStorage.getItem("selectedUser");

  const [user, setUser] = useContext(UserContext); //eslint-disable-line

  return (
    <div>
      <HeaderContainer
        includeLoginTab={true}
        loggedIn={user.isAuthenticated}
        displayName={user.displayName}
      />
      <EditRescuerFormContainer
        selectedUser={selectedUser}
        selectedUserID={selectedUserID}
      />
    </div>
  );
};

export default EditRescuerPage;
