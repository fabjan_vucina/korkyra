import React, { useContext, useEffect, useState } from "react";
import HeaderContainer from "../containers/HeaderContainer/HeaderContainer.js";
import MissionMenuComponent from "../components/MissionMenuComponent/MissionMenuComponent";
import UserContext from "../contexts/UserContext";

const ManageMissionPage = () => {
  const selectedMissionID = localStorage.getItem("selectedMissionID");

  const [fetchedInfo, setFetchedInfo] = useState({});
  const [loadingInfo, setLoadingInfo] = useState(true);

  useEffect(() => {
    fetch("/api/missionInfo/" + selectedMissionID)
      .then(async (response) => {
        const data = await response.json();

        if (!response.ok) {
          const error = (data && data.message) || response.status;
          alert(error);
          return Promise.reject(error);
        }

        setFetchedInfo(data);
        setLoadingInfo(false);
      })
      .catch((error) => {
        console.error("There was an error!", error);
      });
  }, [selectedMissionID]);

  const [user, setUser] = useContext(UserContext); //eslint-disable-line

  return (
    <div>
      <HeaderContainer
        includeLoginTab={true}
        loggedIn={user.isAuthenticated}
        displayName={user.displayName}
      />
      <MissionMenuComponent
        fetchedInfo={fetchedInfo}
        loadingInfo={loadingInfo}
      />
    </div>
  );
};

export default ManageMissionPage;
