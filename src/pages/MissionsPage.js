import React, { useState, useEffect, useContext } from "react";
import HeaderContainer from "../containers/HeaderContainer/HeaderContainer";
import MissionsListContainer from "../containers/MissionsListContainer/MissionsListContainer";
import UserContext from "../contexts/UserContext";

const MissionsPage = () => {
  const [fetchedMissions, setFetchedMissions] = useState([]);
  const [loadingMissions, setLoadingMissions] = useState(true);

  const [user, setUser] = useContext(UserContext); //eslint-disable-line

  useEffect(() => {
    fetch("/api/missions")
      .then(async (response) => {
        const data = await response.json();

        if (!response.ok) {
          const error = (data && data.message) || response.status;
          alert(error);
          return Promise.reject(error);
        }

        setFetchedMissions(data);
        setLoadingMissions(false);
      })
      .catch((error) => {
        console.error("There was an error!", error);
      });
  }, []);

  return (
    <>
      <HeaderContainer
        includeLoginTab={true}
        loggedIn={user.isAuthenticated}
        displayName={user.displayName}
      />
      <MissionsListContainer
        fetchedMissions={fetchedMissions}
        loadingMissions={loadingMissions}
      />
    </>
  );
};

export default MissionsPage;
