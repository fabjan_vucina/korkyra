import React, { useContext, useEffect, useState } from "react";
import HeaderContainer from "../containers/HeaderContainer/HeaderContainer.js";
import UserContext from "../contexts/UserContext";
import StationRescuersListContainer from "../containers/StationRescuersListContainer/StationRescuersListContainer";

const StationRescuersPage = () => {
  const [fetchedStationRescuers, setFetchedStationRescuers] = useState([]);
  const [loadingStationRescuers, setLoadingStationRescuers] = useState(true);

  const [user, setUser] = useContext(UserContext); //eslint-disable-line

  useEffect(() => {
    fetch("/api/stationRescuers/" + user.id_station_member)
      .then(async (response) => {
        const data = await response.json();

        if (!response.ok) {
          const error = (data && data.message) || response.status;
          alert(error);
          return Promise.reject(error);
        }

        setFetchedStationRescuers(data);
        setLoadingStationRescuers(false);
      })
      .catch((error) => {
        console.error("There was an error!", error);
      });
  }, [user.id_station_member]);

  return (
    <div>
      <HeaderContainer
        includeLoginTab={true}
        loggedIn={user.isAuthenticated}
        displayName={user.displayName}
      />
      <StationRescuersListContainer
        fetchedStationRescuers={fetchedStationRescuers}
        loadingStationRescuers={loadingStationRescuers}
      />
    </div>
  );
};

export default StationRescuersPage;
