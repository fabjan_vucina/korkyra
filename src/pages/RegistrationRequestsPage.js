import React, { useState, useEffect, useContext } from "react";
import HeaderContainer from "../containers/HeaderContainer/HeaderContainer";
import RegistrationRequestsListContainer from "../containers/RegistrationRequestsListContainer/RegistrationRequestsListContainer";
import UserContext from "../contexts/UserContext";

const RegistrationRequestsPage = () => {
  const [fetchedRequests, setFetchedRequests] = useState([]);
  const [loadingRequests, setLoadingRequests] = useState(true);

  const [user, setUser] = useContext(UserContext); //eslint-disable-line

  useEffect(() => {
    fetch("/api/registrationRequests")
      .then(async (response) => {
        const data = await response.json();

        if (!response.ok) {
          const error = (data && data.message) || response.status;
          alert(error);
          return Promise.reject(error);
        }

        setFetchedRequests(data);
        setLoadingRequests(false);
      })
      .catch((error) => {
        console.error("There was an error!", error);
      });
  }, []);

  return (
    <>
      <HeaderContainer
        includeLoginTab={true}
        loggedIn={user.isAuthenticated}
        displayName={user.displayName}
      />
      <RegistrationRequestsListContainer
        fetchedRequests={fetchedRequests}
        loadingRequests={loadingRequests}
      />
    </>
  );
};

export default RegistrationRequestsPage;
