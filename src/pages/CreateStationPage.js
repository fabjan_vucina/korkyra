import React, { useContext } from "react";
import HeaderContainer from "../containers/HeaderContainer/HeaderContainer.js";
import CreateStationFormContainer from "../containers/CreateStationFormContainer/CreateStationFormContainer.js";
import UserContext from "../contexts/UserContext";

const CreateStationPage = () => {
  const [user, setUser] = useContext(UserContext); //eslint-disable-line

  return (
    <div>
      <HeaderContainer
        includeLoginTab={true}
        loggedIn={user.isAuthenticated}
        displayName={user.displayName}
      />
      <CreateStationFormContainer />
    </div>
  );
};

export default CreateStationPage;
