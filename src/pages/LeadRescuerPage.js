import React, { useContext } from "react";
import LeadRescuerMenuContainer from "../containers/LeadRescuerMenuContainer/LeadRescuerMenuContainer";
import HeaderContainer from "../containers/HeaderContainer/HeaderContainer";
import UserContext from "../contexts/UserContext";

const LeadRescuerPage = () => {
  const [user, setUser] = useContext(UserContext); //eslint-disable-line

  return (
    <>
      <HeaderContainer
        includeLoginTab={true}
        loggedIn={user.isAuthenticated}
        displayName={user.displayName}
      />
      <LeadRescuerMenuContainer />
    </>
  );
};

export default LeadRescuerPage;
