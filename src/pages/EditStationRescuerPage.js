import React, { useContext } from "react";
import HeaderContainer from "../containers/HeaderContainer/HeaderContainer";
import UserContext from "../contexts/UserContext";
import EditStationRescuerFormContainer from "../containers/EditStationRescuerFormContainer/EditStationRescuerFormContainer";

const EditStationRescuerPage = () => {
  const selectedRescuerID = localStorage.getItem("selectedRescuerID");
  const selectedRescuer = localStorage.getItem("selectedRescuer");

  const [user, setUser] = useContext(UserContext); //eslint-disable-line

  return (
    <>
      <HeaderContainer
        includeLoginTab={true}
        loggedIn={user.isAuthenticated}
        displayName={user.displayName}
      />
      <EditStationRescuerFormContainer
        selectedRescuerID={selectedRescuerID}
        selectedRescuer={selectedRescuer}
      />
    </>
  );
};

export default EditStationRescuerPage;
