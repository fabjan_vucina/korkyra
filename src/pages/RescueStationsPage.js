import React, { useState, useEffect, useContext } from "react";
import HeaderContainer from "../containers/HeaderContainer/HeaderContainer";
import StationsListContainer from "../containers/StationsListContainer/StationsListContainer";
import UserContext from "../contexts/UserContext";

const RescueStationsPage = () => {
  const [fetchedStations, setFetchedStations] = useState([]);
  const [loadingStations, setLoadingStations] = useState(true);

  const [user, setUser] = useContext(UserContext); //eslint-disable-line

  useEffect(() => {
    fetch("/api/stations")
      .then(async (response) => {
        const data = await response.json();

        if (!response.ok) {
          const error = (data && data.message) || response.status;
          alert(error);
          return Promise.reject(error);
        }

        setFetchedStations(data);
        setLoadingStations(false);
      })
      .catch((error) => {
        console.error("There was an error!", error);
      });
  }, []);

  return (
    <>
      <HeaderContainer
        includeLoginTab={true}
        loggedIn={user.isAuthenticated}
        displayName={user.displayName}
      />
      <StationsListContainer
        fetchedStations={fetchedStations}
        loadingStations={loadingStations}
      />
    </>
  );
};

export default RescueStationsPage;
