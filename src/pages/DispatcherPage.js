import React, { useContext } from "react";
import HeaderContainer from "../containers/HeaderContainer/HeaderContainer.js";
import UserContext from "../contexts/UserContext";
import DispatcherMenuContainer from "../containers/DispatcherMenuContainer/DispatcherMenuContainer";

const DispatcherPage = () => {
  const [user, setUser] = useContext(UserContext); //eslint-disable-line

  return (
    <div>
      <HeaderContainer
        includeLoginTab={true}
        loggedIn={user.isAuthenticated}
        displayName={user.displayName}
      />
      <DispatcherMenuContainer />
    </div>
  );
};

export default DispatcherPage;
