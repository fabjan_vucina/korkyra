import React from "react";
import HeaderContainer from "../containers/HeaderContainer/HeaderContainer.js";
import LoginFormContainer from "../containers/LoginFormContainer/LoginFormContainer.js";

const LoginPage = () => {
  return (
    <div>
      <HeaderContainer
        includeLoginTab={false}
        loggedIn={false}
        displayName={"John Doe"}
      />
      <LoginFormContainer />
    </div>
  );
};

export default LoginPage;
