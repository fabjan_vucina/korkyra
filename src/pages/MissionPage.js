import React, { useContext } from "react";
import HeaderContainer from "../containers/HeaderContainer/HeaderContainer.js";
import UserContext from "../contexts/UserContext";
import MissionInteractContainer from "../containers/MissionInteractContainer/MissionInteractContainer";

const MissionPage = () => {
  const [user, setUser] = useContext(UserContext); //eslint-disable-line

  const participatingMissionID = localStorage.getItem("participatingMissionID"); //eslint-disable-line

  return (
    <div>
      <HeaderContainer
        includeLoginTab={true}
        loggedIn={user.isAuthenticated}
        displayName={user.displayName}
      />
      <MissionInteractContainer
        participatingMissionID={participatingMissionID}
      />
    </div>
  );
};

export default MissionPage;
