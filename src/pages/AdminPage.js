import React, { useContext } from "react";
import AdminMenuContainer from "../containers/AdminMenuContainer/AdminMenuContainer";
import HeaderContainer from "../containers/HeaderContainer/HeaderContainer";
import UserContext from "../contexts/UserContext";

const AdminPage = () => {
  const [user, setUser] = useContext(UserContext); //eslint-disable-line

  return (
    <>
      <HeaderContainer
        includeLoginTab={true}
        loggedIn={user.isAuthenticated}
        displayName={user.displayName}
      />
      <AdminMenuContainer />
    </>
  );
};

export default AdminPage;
