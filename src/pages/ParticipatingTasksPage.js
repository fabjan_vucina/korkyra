import React, { useState, useEffect, useContext } from "react";
import HeaderContainer from "../containers/HeaderContainer/HeaderContainer";
import ParticipatingTasksListContainer from "../containers/ParticipatingTasksListContainer/ParticipatingTasksListContainer";
import UserContext from "../contexts/UserContext";

const ParticipatingTasksPage = () => {
  const [fetchedParticipatingTasks, setFetchedParticipatingTasks] = useState(
    []
  );
  const [loadingParticipatingTasks, setLoadingParticipatingTasks] = useState(
    true
  );

  const [user, setUser] = useContext(UserContext); //eslint-disable-line

  useEffect(() => {
    fetch("/api/participatingTasks/" + user.id_user)
      .then(async (response) => {
        const data = await response.json();

        if (!response.ok) {
          const error = (data && data.message) || response.status;
          alert(error);
          return Promise.reject(error);
        }

        setFetchedParticipatingTasks(data);
        setLoadingParticipatingTasks(false);
      })
      .catch((error) => {
        console.error("There was an error!", error);
      });
  }, [user.id_user]);

  return (
    <>
      <HeaderContainer
        includeLoginTab={true}
        loggedIn={user.isAuthenticated}
        displayName={user.displayName}
      />
      <ParticipatingTasksListContainer
        fetchedParticipatingTasks={fetchedParticipatingTasks}
        loadingParticipatingTasks={loadingParticipatingTasks}
      />
    </>
  );
};

export default ParticipatingTasksPage;
