import React, { useState, useEffect, useContext } from "react";
import HeaderContainer from "../containers/HeaderContainer/HeaderContainer";
import HelpRequestsListContainer from "../containers/HelpRequestsListContainer/HelpRequestsListContainer";
import UserContext from "../contexts/UserContext";

const HelpRequestsPage = () => {
  const [fetchedHelpRequests, setFetchedHelpRequests] = useState([]);
  const [loadingHelpRequests, setLoadingHelpRequests] = useState(true);

  const [user, setUser] = useContext(UserContext); //eslint-disable-line

  useEffect(() => {
    fetch("/api/helpRequests/" + user.id_user)
      .then(async (response) => {
        const data = await response.json();

        if (!response.ok) {
          const error = (data && data.message) || response.status;
          alert(error);
          return Promise.reject(error);
        }

        setFetchedHelpRequests(data);
        setLoadingHelpRequests(false);
      })
      .catch((error) => {
        console.error("There was an error!", error);
      });
  }, [user.id_user]);

  return (
    <>
      <HeaderContainer
        includeLoginTab={true}
        loggedIn={user.isAuthenticated}
        displayName={user.displayName}
      />
      <HelpRequestsListContainer
        fetchedHelpRequests={fetchedHelpRequests}
        loadingHelpRequests={loadingHelpRequests}
      />
    </>
  );
};

export default HelpRequestsPage;
