import React, { useState, useEffect, useContext } from "react";
import HeaderContainer from "../containers/HeaderContainer/HeaderContainer";
import UsersListContainer from "../containers/UsersListContainer/UsersListContainer";
import UserContext from "../contexts/UserContext";

const ApprovedUsersPage = () => {
  const [fetchedUsers, setFetchedUsers] = useState([]);
  const [loadingUsers, setLoadingUsers] = useState(true);

  const [user, setUser] = useContext(UserContext); //eslint-disable-line

  useEffect(() => {
    fetch("/api/authenticatedUsers")
      .then(async (response) => {
        const data = await response.json();

        if (!response.ok) {
          const error = (data && data.message) || response.status;
          alert(error);
          return Promise.reject(error);
        }

        setFetchedUsers(data);
        setLoadingUsers(false);
      })
      .catch((error) => {
        console.error("There was an error!", error);
      });
  }, []);

  return (
    <>
      <HeaderContainer
        includeLoginTab={true}
        loggedIn={user.isAuthenticated}
        displayName={user.displayName}
      />
      <UsersListContainer
        fetchedUsers={fetchedUsers}
        loadingUsers={loadingUsers}
      />
    </>
  );
};

export default ApprovedUsersPage;
