import React from "react";
import WelcomeComponent from "../components/WelcomeComponent/WelcomeComponent.js";
import HeaderContainer from "../containers/HeaderContainer/HeaderContainer.js";

const WelcomePage = () => {
  return (
    <>
      <HeaderContainer
        includeLoginTab={true}
        loggedIn={false}
        displayName={"John Doe"}
      />
      <WelcomeComponent />
    </>
  );
};

export default WelcomePage;
