import React, { useContext } from "react";
import HeaderContainer from "../containers/HeaderContainer/HeaderContainer.js";
import EditDispactherFormContainer from "../containers/EditDispatcherFormContainer/EditDispatcherFormContainer.js";
import UserContext from "../contexts/UserContext";

const EditDispatcherPage = () => {
  const selectedUserID = localStorage.getItem("selectedUserID");
  const selectedUser = localStorage.getItem("selectedUser");

  const [user, setUser] = useContext(UserContext); //eslint-disable-line

  return (
    <div>
      <HeaderContainer
        includeLoginTab={true}
        loggedIn={user.isAuthenticated}
        displayName={user.displayName}
      />
      <EditDispactherFormContainer
        selectedUser={selectedUser}
        selectedUserID={selectedUserID}
      />
    </div>
  );
};

export default EditDispatcherPage;
