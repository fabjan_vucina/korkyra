import React, { useContext } from "react";
import HeaderContainer from "../containers/HeaderContainer/HeaderContainer.js";
import CreateMissionFormContainer from "../containers/CreateMissionFormContainer/CreateMissionFormContainer.js";
import UserContext from "../contexts/UserContext";

const CreateMissionPage = () => {
    const [user, setUser] = useContext(UserContext); //eslint-disable-line

    return (
        <div>
            <HeaderContainer
                includeLoginTab={true}
                loggedIn={user.isAuthenticated}
                displayName={user.displayName}
            />
            <CreateMissionFormContainer />
        </div>
    );
};

export default CreateMissionPage;
