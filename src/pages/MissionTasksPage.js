import React, { useState, useEffect, useContext } from "react";
import HeaderContainer from "../containers/HeaderContainer/HeaderContainer";
import MissionTasksListContainer from "../containers/MissionTasksListContainer/MissionTasksListContainer";
import UserContext from "../contexts/UserContext";

const MissionTasksPage = () => {
  const selectedMissionID = localStorage.getItem("selectedMissionID");

  const [fetchedMissionTasks, setFetchedMissionTasks] = useState([]);
  const [loadingMissionTasks, setLoadingMissionTasks] = useState(true);

  const [user, setUser] = useContext(UserContext); //eslint-disable-line

  useEffect(() => {
    fetch("/api/missionTasks/" + selectedMissionID)
      .then(async (response) => {
        const data = await response.json();

        if (!response.ok) {
          const error = (data && data.message) || response.status;
          alert(error);
          return Promise.reject(error);
        }

        setFetchedMissionTasks(data);
        setLoadingMissionTasks(false);
      })
      .catch((error) => {
        console.error("There was an error!", error);
      });
  }, [selectedMissionID]);

  return (
    <>
      <HeaderContainer
        includeLoginTab={true}
        loggedIn={user.isAuthenticated}
        displayName={user.displayName}
      />
      <MissionTasksListContainer
        fetchedMissionTasks={fetchedMissionTasks}
        loadingMissionTasks={loadingMissionTasks}
      />
    </>
  );
};

export default MissionTasksPage;
