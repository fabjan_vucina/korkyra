import React, { useContext } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import WelcomePage from "../pages/WelcomePage";
import LoginPage from "../pages/LoginPage";
import RegisterPage from "../pages/RegisterPage";
import AdminPage from "../pages/AdminPage";
import RegistrationRequestsPage from "../pages/RegistrationRequestsPage";
import ApprovedUsersPage from "../pages/ApprovedUsersPage";
import EditRescuerPage from "../pages/EditRescuerPage";
import EditDispatcherPage from "../pages/EditDispatcherPage";
import EditStationPage from "../pages/EditStationPage";
import RescueStationsPage from "../pages/RescueStationsPage";
import CreateStationPage from "../pages/CreateStationPage";
import DispatcherPage from "../pages/DispatcherPage";
import CreateMissionPage from "../pages/CreateMissionPage";
import MissionsPage from "../pages/MissionsPage";
import CreateMissionTaskPage from "../pages/CreateMissionTaskPage";
import RescuerPage from "../pages/RescuerPage";
import LeadRescuerPage from "../pages/LeadRescuerPage";
import StationRescuersPage from "../pages/StationRescuersPage";
import EditStationRescuerPage from "../pages/EditStationRescuerPage";
import HelpRequestsPage from "../pages/HelpRequestsPage";
import ParticipatingTasksPage from "../pages/ParticipatingTasksPage";
import MissionPage from "../pages/MissionPage";
import UserContext from "../contexts/UserContext";
import ManageMissionPage from "../pages/ManageMissionPage";
import MissionTasksPage from "../pages/MissionTasksPage";

const PageRouter = () => {
  const [user] = useContext(UserContext);

  return (
    <Router>
      <Switch>
        <Route exact path="/">
          {!user.isAuthenticated ? (
            <WelcomePage />
          ) : user.isLeadRescuer ? (
            <Redirect to="/leadRescuer" />
          ) : user.role == "Rescuer" ? (
            <Redirect to="/rescuer" />
          ) : user.role == "Dispatcher" ? (
            <Redirect to="/dispatcher" />
          ) : (
            <Redirect to="/admin" />
          )}
        </Route>

        <Route exact path="/login">
          {!user.isAuthenticated ? (
            <LoginPage />
          ) : user.isLeadRescuer ? (
            <Redirect to="/leadRescuer" />
          ) : user.role == "Rescuer" ? (
            <Redirect to="/rescuer" />
          ) : user.role == "Dispatcher" ? (
            <Redirect to="/dispatcher" />
          ) : (
            <Redirect to="/admin" />
          )}
        </Route>

        <Route exact path="/register">
          {!user.isAuthenticated ? (
            <RegisterPage />
          ) : user.isLeadRescuer ? (
            <Redirect to="/leadRescuer" />
          ) : user.role == "Rescuer" ? (
            <Redirect to="/rescuer" />
          ) : user.role == "Dispatcher" ? (
            <Redirect to="/dispatcher" />
          ) : (
            <Redirect to="/admin" />
          )}
        </Route>

        <Route exact path="/admin">
          {user.isAuthenticated && user.role == "Administrator" ? (
            <AdminPage />
          ) : !user.isAuthenticated ? (
            <Redirect to="/login" />
          ) : user.isLeadRescuer ? (
            <Redirect to="/leadRescuer" />
          ) : user.role == "rescuer" ? (
            <Redirect to="/rescuer" />
          ) : (
            <Redirect to="/dispatcher" />
          )}
        </Route>

        <Route exact path="/registrationRequests">
          {user.isAuthenticated && user.role == "Administrator" ? (
            <RegistrationRequestsPage />
          ) : !user.isAuthenticated ? (
            <Redirect to="/login" />
          ) : user.isLeadRescuer ? (
            <Redirect to="/leadRescuer" />
          ) : user.role == "Rescuer" ? (
            <Redirect to="/rescuer" />
          ) : (
            <Redirect to="/dispatcher" />
          )}
        </Route>

        <Route exact path="/approvedUsers">
          {user.isAuthenticated && user.role == "Administrator" ? (
            <ApprovedUsersPage />
          ) : !user.isAuthenticated ? (
            <Redirect to="/login" />
          ) : user.isLeadRescuer ? (
            <Redirect to="/leadRescuer" />
          ) : user.role == "Rescuer" ? (
            <Redirect to="/rescuer" />
          ) : (
            <Redirect to="/dispatcher" />
          )}
        </Route>

        <Route exact path="/changeRescuerInfo">
          {user.isAuthenticated && user.role == "Administrator" ? (
            <EditRescuerPage />
          ) : !user.isAuthenticated ? (
            <Redirect to="/login" />
          ) : user.isLeadRescuer ? (
            <Redirect to="/leadRescuer" />
          ) : user.role == "Rescuer" ? (
            <Redirect to="/rescuer" />
          ) : (
            <Redirect to="/dispatcher" />
          )}
        </Route>

        <Route exact path="/changeDispatcherInfo">
          {user.isAuthenticated && user.role == "Administrator" ? (
            <EditDispatcherPage />
          ) : !user.isAuthenticated ? (
            <Redirect to="/login" />
          ) : user.isLeadRescuer ? (
            <Redirect to="/leadRescuer" />
          ) : user.role == "Rescuer" ? (
            <Redirect to="/rescuer" />
          ) : (
            <Redirect to="/dispatcher" />
          )}
        </Route>

        <Route exact path="/rescueStations">
          {user.isAuthenticated && user.role == "Administrator" ? (
            <RescueStationsPage />
          ) : !user.isAuthenticated ? (
            <Redirect to="/login" />
          ) : user.isLeadRescuer ? (
            <Redirect to="/leadRescuer" />
          ) : user.role == "Rescuer" ? (
            <Redirect to="/rescuer" />
          ) : (
            <Redirect to="/dispatcher" />
          )}
        </Route>

        <Route exact path="/changeStationInfo">
          {user.isAuthenticated && user.role == "Administrator" ? (
            <EditStationPage />
          ) : !user.isAuthenticated ? (
            <Redirect to="/login" />
          ) : user.isLeadRescuer ? (
            <Redirect to="/leadRescuer" />
          ) : user.role == "Rescuer" ? (
            <Redirect to="/rescuer" />
          ) : (
            <Redirect to="/dispatcher" />
          )}
        </Route>

        <Route exact path="/createStation">
          {user.isAuthenticated && user.role == "Administrator" ? (
            <CreateStationPage />
          ) : !user.isAuthenticated ? (
            <Redirect to="/login" />
          ) : user.isLeadRescuer ? (
            <Redirect to="/leadRescuer" />
          ) : user.role == "Rescuer" ? (
            <Redirect to="/rescuer" />
          ) : (
            <Redirect to="/dispatcher" />
          )}
        </Route>

        <Route exact path="/rescuer">
          {user.isAuthenticated &&
          user.role == "Rescuer" &&
          !user.isLeadRescuer ? (
            <RescuerPage />
          ) : !user.isAuthenticated ? (
            <Redirect to="/login" />
          ) : user.isLeadRescuer ? (
            <Redirect to="/leadRescuer" />
          ) : user.role == "Administrator" ? (
            <Redirect to="/admin" />
          ) : (
            <Redirect to="/dispatcher" />
          )}
        </Route>

        <Route exact path="/helpRequests">
          {user.isAuthenticated && user.role == "Rescuer" ? (
            <HelpRequestsPage />
          ) : !user.isAuthenticated ? (
            <Redirect to="/login" />
          ) : user.role == "Administrator" ? (
            <Redirect to="/admin" />
          ) : (
            <Redirect to="/dispatcher" />
          )}
        </Route>

        <Route exact path="/participatingMissionTasks">
          {user.isAuthenticated && user.role == "Rescuer" ? (
            <ParticipatingTasksPage />
          ) : !user.isAuthenticated ? (
            <Redirect to="/login" />
          ) : user.role == "Administrator" ? (
            <Redirect to="/admin" />
          ) : (
            <Redirect to="/dispatcher" />
          )}
        </Route>

        <Route exact path="/mission">
          {user.isAuthenticated && user.role == "Rescuer" ? (
            <MissionPage />
          ) : !user.isAuthenticated ? (
            <Redirect to="/login" />
          ) : user.role == "Administrator" ? (
            <Redirect to="/admin" />
          ) : (
            <Redirect to="/dispatcher" />
          )}
        </Route>

        <Route exact path="/dispatcher">
          {user.isAuthenticated && user.role == "Dispatcher" ? (
            <DispatcherPage />
          ) : !user.isAuthenticated ? (
            <Redirect to="/login" />
          ) : user.isLeadRescuer ? (
            <Redirect to="/leadRescuer" />
          ) : user.role == "Rescuer" ? (
            <Redirect to="/rescuer" />
          ) : (
            <Redirect to="/admin" />
          )}
        </Route>

        <Route exact path="/createMission">
          {user.isAuthenticated && user.role == "Dispatcher" ? (
            <CreateMissionPage />
          ) : !user.isAuthenticated ? (
            <Redirect to="/login" />
          ) : user.isLeadRescuer ? (
            <Redirect to="/leadRescuer" />
          ) : user.role == "Rescuer" ? (
            <Redirect to="/rescuer" />
          ) : (
            <Redirect to="/admin" />
          )}
        </Route>

        <Route exact path="/missions">
          {user.isAuthenticated && user.role == "Dispatcher" ? (
            <MissionsPage />
          ) : !user.isAuthenticated ? (
            <Redirect to="/login" />
          ) : user.isLeadRescuer ? (
            <Redirect to="/leadRescuer" />
          ) : user.role == "Rescuer" ? (
            <Redirect to="/rescuer" />
          ) : (
            <Redirect to="/admin" />
          )}
        </Route>

        <Route exact path="/createMissionTask">
          {user.isAuthenticated && user.role == "Dispatcher" ? (
            <CreateMissionTaskPage />
          ) : !user.isAuthenticated ? (
            <Redirect to="/login" />
          ) : user.isLeadRescuer ? (
            <Redirect to="/leadRescuer" />
          ) : user.role == "Rescuer" ? (
            <Redirect to="/rescuer" />
          ) : (
            <Redirect to="/admin" />
          )}
        </Route>

        <Route exact path="/manageMission">
          {user.isAuthenticated && user.role == "Dispatcher" ? (
            <ManageMissionPage />
          ) : !user.isAuthenticated ? (
            <Redirect to="/login" />
          ) : user.isLeadRescuer ? (
            <Redirect to="/leadRescuer" />
          ) : user.role == "Rescuer" ? (
            <Redirect to="/rescuer" />
          ) : (
            <Redirect to="/admin" />
          )}
        </Route>

        <Route exact path="/missionTasks">
          {user.isAuthenticated && user.role == "Dispatcher" ? (
            <MissionTasksPage />
          ) : !user.isAuthenticated ? (
            <Redirect to="/login" />
          ) : user.isLeadRescuer ? (
            <Redirect to="/leadRescuer" />
          ) : user.role == "Rescuer" ? (
            <Redirect to="/rescuer" />
          ) : (
            <Redirect to="/admin" />
          )}
        </Route>

        <Route exact path="/leadRescuer">
          {user.isAuthenticated && user.isLeadRescuer ? (
            <LeadRescuerPage />
          ) : !user.isAuthenticated ? (
            <Redirect to="/login" />
          ) : user.role == "Administrator" ? (
            <Redirect to="/admin" />
          ) : user.role == "Rescuer" ? (
            <Redirect to="/rescuer" />
          ) : (
            <Redirect to="/dispatcher" />
          )}
        </Route>

        <Route exact path="/stationRescuers">
          {user.isAuthenticated && user.isLeadRescuer ? (
            <StationRescuersPage />
          ) : !user.isAuthenticated ? (
            <Redirect to="/login" />
          ) : user.role == "Administrator" ? (
            <Redirect to="/admin" />
          ) : user.role == "Rescuer" ? (
            <Redirect to="/rescuer" />
          ) : (
            <Redirect to="/dispatcher" />
          )}
        </Route>

        <Route exact path="/changeStationRescuerInfo">
          {user.isAuthenticated && user.isLeadRescuer ? (
            <EditStationRescuerPage />
          ) : !user.isAuthenticated ? (
            <Redirect to="/login" />
          ) : user.isLeadRescuer ? (
            <Redirect to="/leadRescuer" />
          ) : user.role == "Rescuer" ? (
            <Redirect to="/rescuer" />
          ) : (
            <Redirect to="/dispatcher" />
          )}
        </Route>
      </Switch>
    </Router>
  );
};

export default PageRouter;
