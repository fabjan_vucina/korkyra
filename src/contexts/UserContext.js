import { createContext } from "react";

const UserContext = createContext([
  {
    isAuthenticated: false,
    id_user: "",
    displayName: "",
    role: "",
    isLeadRescuer: false,
    id_station_member: "",
  },
]);

export default UserContext;
