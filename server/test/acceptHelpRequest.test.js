const acceptHelpRequest = require('./functions/acceptHelpRequest')

test("Accepting a help request should be responded with a success message", async () => {
  expect.assertions(1);
  const data = await acceptHelpRequest();
  return expect(data.message).toEqual('You have accepted the help request.');
});