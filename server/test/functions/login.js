const fetch = require("node-fetch");

async function login(username, password) {
    const url = "http://localhost:5000/api/login";
    const requestOptions = {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
          username: username,
          password: password,
        }),
    };

    const response = await fetch(url, requestOptions);
    const data = await response.json();

    return data;
}

module.exports = login;