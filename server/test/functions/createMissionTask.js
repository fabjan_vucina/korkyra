const fetch = require("node-fetch");

async function createMissionTask() {
    const url = "http://localhost:5000/api/createMissionTask";
    const requestOptions = {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
          id_mission: 2,
          id_task_type: 1,
          id_transportation_type: 1
        }),
    };

    const response = await fetch(url, requestOptions);
    const data = await response.json();

    return data;
}

module.exports = createMissionTask;