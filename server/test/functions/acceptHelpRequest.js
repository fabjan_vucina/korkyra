const fetch = require("node-fetch");

async function acceptHelpRequest() {
    const url = "http://localhost:5000/api/acceptHelpRequest";
    const requestOptions = {
        method: "PUT",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
          id_user: 11,
          id_mission_task: 12
        }),
    };

    const response = await fetch(url, requestOptions);
    const data = await response.json();

    return data;
}

module.exports = acceptHelpRequest;