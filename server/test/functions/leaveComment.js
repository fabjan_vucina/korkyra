const fetch = require("node-fetch");

async function leaveComment() {
    const url = "http://localhost:5000/api/leaveComment";
    const requestOptions = {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
          id_user: 11,
          id_mission: 2,
          comment: "I need dry clothes for 2 male adults."
        }),
    };

    const response = await fetch(url, requestOptions);
    const data = await response.json();

    return data;
}

module.exports = leaveComment;