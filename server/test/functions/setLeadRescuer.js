const fetch = require("node-fetch");
const adminQueries = require("../../db/adminQueries");

async function setLeadRescuer() {
    await adminQueries.assignRescuerToStation(11, 2);

    const url = "http://localhost:5000/api/changeStationInfo";
    const requestOptions = {
        method: "PUT",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
          id_station: 2,
          id_lead_rescuer: 11
        }),
    };

    const response = await fetch(url, requestOptions);
    const data = await response.json();

    return data;
}

module.exports = setLeadRescuer;