const fetch = require("node-fetch");

async function approveRegistrationRequest() {
    const url = "http://localhost:5000/api/approveRequest";
    const requestOptions = {
        method: "PUT",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
          id_user: 11
        }),
    };

    const response = await fetch(url, requestOptions);
    const data = await response.json();

    return data;
}

module.exports = approveRegistrationRequest;