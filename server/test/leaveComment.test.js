const leaveComment = require('./functions/leaveComment')

test("Leaving a comment should be responded with a success message", async () => {
  expect.assertions(1);
  const data = await leaveComment();
  return expect(data.message)
            .toEqual('You have left the comment: I need dry clothes for 2 male adults.');
});
