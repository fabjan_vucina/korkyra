const approveRegistrationRequest = require('./functions/approveRegistrationRequest')

test("Registration approval request should be responded with a success message", async () => {
  expect.assertions(1);
  const data = await approveRegistrationRequest();
  return expect(data.message).toEqual('You have approved access to Marko Knežević.');
});