const login = require('./functions/login')

describe("Login request should be responded with", () => {
  test("non-existent username error message", async () => {
    expect.assertions(1);
    const data = await login('johndoe', '12345');
    return expect(data.message).toEqual('This username doesn\'t exist!');
  });
  
  test("incorrect password error message", async () => {
    expect.assertions(1);
    const data = await login('ivanhorvat', '12346');
    return expect(data.message).toEqual('Incorrect password for username: ivanhorvat');
  });
  
  test("user details", async () => {
    expect.assertions(1);
    const data = await login('ivanhorvat', '12345');
    return expect(data.id_user).toBe(1);
  });
});
