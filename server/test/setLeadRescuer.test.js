const setLeadRescuer = require('./functions/setLeadRescuer')

test("Setting a station rescuer request should be responded with a success message", async () => {
  expect.assertions(1);
  const data = await setLeadRescuer();
  return expect(data.message)
            .toEqual('You have updated station: Mosor. The new lead rescuer is: Marko Knežević.');
});
