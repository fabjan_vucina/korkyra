const createMissionTask = require('./functions/createMissionTask')

test("Creating a new mission task should be responded with a success message", async () => {
  expect.assertions(1);
  const data = await createMissionTask();
  return expect(data.message).toEqual('You have created a new mission task.');
});