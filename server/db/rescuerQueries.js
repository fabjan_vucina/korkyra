const db = require(".");

module.exports = {
  getHelpRequests: async (id_user) => {
    const sql = `SELECT id_mission_task, task_name, transportation_name
                        FROM mission_task NATURAL JOIN mission
                                          NATURAL JOIN task_type 
                                          NATURAL JOIN transportation_type
                        WHERE isfinished = false
                            AND id_rescuer IS NULL
                            AND id_station = (SELECT id_station_member FROM users
                                                WHERE id_user = ${id_user})
                            AND id_transportation_type IN (SELECT id_transportation_type 
                                                            FROM transportation_eligibility
                                                            WHERE id_rescuer = ${id_user});`;

    try {
      const result = await db.query(sql, []);
      return result.rows;
    } catch (err) {
      console.log(err);
      throw err;
    }
  },

  acceptHelpRequest: async (id_user, id_mission_task) => {
    let sqlUpdate = `UPDATE mission_task
                            SET id_rescuer = ${id_user}
                            WHERE id_mission_task = ${id_mission_task};`;

    try {
      await db.query(sqlUpdate, []);
      return;
    } catch (err) {
      console.log(err);
      throw err;
    }
  },

  getParticipatingTasks: async (id_user) => {
    const sql = `SELECT id_mission, id_mission_task, task_name, transportation_name
                        FROM mission_task NATURAL JOIN task_type 
                                          NATURAL JOIN transportation_type
                        WHERE isfinished = false
                            AND id_rescuer =  ${id_user};`;

    try {
      const result = await db.query(sql, []);
      return result.rows;
    } catch (err) {
      console.log(err);
      throw err;
    }
  },

  getMissionColleagues: async (id_user, id_mission) => {
    const sql = `SELECT id_user, firstname, lastname
                        FROM users JOIN mission_task  
                            ON id_user = id_rescuer
                        WHERE isfinished = false
                            AND id_rescuer <>  ${id_user}
                            AND id_mission = ${id_mission};`;

    try {
      const result = await db.query(sql, []);
      return result.rows;
    } catch (err) {
      console.log(err);
      throw err;
    }
  },

  getMissionComments: async (id_mission) => {
    const sql = `SELECT * FROM comment JOIN users ON id_rescuer = id_user
                    WHERE id_mission = ${id_mission}
                    ORDER BY id_comment;`;

    try {
      const result = await db.query(sql, []);
      return result.rows;
    } catch (err) {
      console.log(err);
      throw err;
    }
  },

  leaveComment: async (id_user, id_mission, comment) => {
    const sqlInsert = `INSERT INTO comment (id_rescuer, id_mission, comment) 
                                VALUES ( ${id_user}, ${id_mission}, '${comment}')
                                RETURNING comment;`;

    try {
      const result = await db.query(sqlInsert, []);
      return result.rows[0];
    } catch (err) {
      console.log(err);
      throw err;
    }
  },

  removeTransportationEligibilities: async (id_user) => {
    const sql = `DELETE FROM transportation_eligibility
                            WHERE id_rescuer = ${id_user};`;

    try {
      await db.query(sql, []);
      return;
    } catch (err) {
      console.log(err);
      throw err;
    }
  },

  assignTransportationTypes: async (id_user, ...transportation_types) => {
    const sqlInsert = `INSERT INTO transportation_eligibility (id_rescuer, id_transportation_type) 
                            VALUES ( ${id_user}, $1);`;

    try {
      for (let i = 0; i < transportation_types.length; i++)
        await db.query(sqlInsert, [transportation_types[i]]);
      return;
    } catch (err) {
      console.log(err);
      throw err;
    }
  },
};
