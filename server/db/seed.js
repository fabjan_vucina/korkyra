const { Pool } = require("pg");

//created connection pool for the database
const pool = new Pool({
  connectionString: process.env.DATABASE_URL,
  ssl: {
    rejectUnauthorized: false,
  },
});

const setupDatabase = `
CREATE EXTENSION postgis;

CREATE TABLE task_type (
    id_task_type SERIAL NOT NULL,
    task_name VARCHAR NOT NULL,
    PRIMARY KEY (id_task_type)
);

INSERT INTO task_type (task_name) VALUES 
    ('Follow specific route'),
    ('Setup temporary station'),
    ('Refresh stock in temporary station'),
    ('Get to a location');

CREATE TABLE user_role (
    id_role SERIAL NOT NULL,
    role VARCHAR NOT NULL,
    PRIMARY KEY (id_role)
);

INSERT INTO user_role (role) VALUES 
    ('Administrator'),
    ('Rescuer'),
    ('Dispatcher');

CREATE TABLE transportation_type (
    id_transportation_type SERIAL NOT NULL,
    transportation_name VARCHAR NOT NULL,
    visibility_radius INT NOT NULL,
    PRIMARY KEY (id_transportation_type)
);

INSERT INTO transportation_type (transportation_name, visibility_radius) VALUES 
    ('On foot', 10),
    ('Bicycle', 20),
    ('Car', 50),
    ('Drone', 100),
    ('Helicopter', 500);

CREATE TABLE users (
    id_user SERIAL NOT NULL,
    username VARCHAR NOT NULL,
    password VARCHAR NOT NULL,
    photo BYTEA,
    firstName VARCHAR NOT NULL,
    lastName VARCHAR NOT NULL,
    id_role INT NOT NULL,
    email VARCHAR NOT NULL,
    phoneNumber VARCHAR NOT NULL,
    geo_location GEOGRAPHY(POINT,4326),
    approved BOOLEAN NOT NULL,
    PRIMARY KEY (id_user),
    FOREIGN KEY (id_role) REFERENCES user_role(id_role) 
        ON DELETE CASCADE
        ON UPDATE CASCADE
);

CREATE TABLE station (
    id_station SERIAL NOT NULL,
    name VARCHAR NOT NULL,
    geo_location GEOGRAPHY(POINT,4326),
    id_lead_rescuer INT,
    PRIMARY KEY (id_station),
    FOREIGN KEY (id_lead_rescuer) REFERENCES users(id_user)
        ON DELETE SET NULL
        ON UPDATE CASCADE
);

INSERT INTO station (name) VALUES 
    ('Medvednica'),
    ('Mosor'),
    ('Biokovo'),
    ('Papuk'),
    ('Paklenica');

ALTER TABLE users ADD COLUMN id_station_member INT;
ALTER TABLE users ADD CONSTRAINT usersFKstation FOREIGN KEY (id_station_member) REFERENCES station(id_station)
                                                    ON DELETE SET NULL
                                                    ON UPDATE CASCADE;

INSERT INTO users (username, password, firstName, lastName, id_role, email, phoneNumber, approved) VALUES 
    ('ivanhorvat', '12345', 'Ivan', 'Horvat', 1, 'ivanhorvat@gmail.com', '098123456', true);

CREATE TABLE mission (
  id_mission SERIAL NOT NULL,
  description VARCHAR NOT NULL,
  id_station INT NOT NULL,
  PRIMARY KEY (id_mission),
  FOREIGN KEY (id_station) REFERENCES station(id_station)
        ON DELETE SET NULL
        ON UPDATE CASCADE
);

CREATE TABLE mission_task (
    id_mission_task SERIAL NOT NULL,
    id_mission INT NOT NULL,
    isFinished BOOLEAN NOT NULL,
    id_transportation_type INT NOT NULL,
    id_task_type INT NOT NULL,
    id_rescuer INT,
    PRIMARY KEY (id_mission_task, id_mission),
    FOREIGN KEY (id_mission) REFERENCES mission(id_mission)
        ON DELETE CASCADE
        ON UPDATE CASCADE,
    FOREIGN KEY (id_transportation_type) REFERENCES transportation_type(id_transportation_type)
        ON DELETE SET NULL
        ON UPDATE CASCADE,
    FOREIGN KEY (id_task_type) REFERENCES task_type(id_task_type)
        ON DELETE SET NULL
        ON UPDATE CASCADE,
    FOREIGN KEY (id_rescuer) REFERENCES users(id_user)
        ON DELETE SET NULL
        ON UPDATE CASCADE
);

CREATE TABLE comment (
    id_comment SERIAL,
    id_mission INT NOT NULL,
    id_rescuer INT NOT NULL,
    comment VARCHAR NOT NULL,
    PRIMARY KEY (id_comment),
    FOREIGN KEY (id_mission) REFERENCES mission(id_mission)
        ON DELETE CASCADE
        ON UPDATE CASCADE,
    FOREIGN KEY (id_rescuer) REFERENCES users(id_user)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);

CREATE TABLE transportation_eligibility (
    id_rescuer INT NOT NULL,
    id_transportation_type INT NOT NULL,
    PRIMARY KEY (id_rescuer, id_transportation_type),
    FOREIGN KEY (id_rescuer) REFERENCES users(id_user)
        ON DELETE CASCADE
        ON UPDATE CASCADE,
    FOREIGN KEY (id_transportation_type) REFERENCES transportation_type(id_transportation_type)
        ON DELETE CASCADE
        ON UPDATE CASCADE 
);
`;

pool.query(setupDatabase, [], (err, result) => {
  if (err) {
    return console.error(err.message);
  }
  console.log("Successful creation of the database");
});
