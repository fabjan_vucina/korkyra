const db = require(".");

module.exports = {
    
    usernameAlreadyExists: async username => {
        const sql = `SELECT id_user FROM users WHERE 
                        username = '${username}';`;
    
        try {
           
            const result = await db.query(sql, []);
            return result.rows == 0 ? false : true;
    
        } catch (err) {
            console.log(err);
            throw err
        }
    },

    emailAlreadyExists: async email => {
        const sql = `SELECT id_user FROM users WHERE 
                        email = '${email}';`;
    
        try {
           
            const result = await db.query(sql, []);
            return result.rows == 0 ? false : true;
    
        } catch (err) {
            console.log(err);
            throw err
        }
    },

    createRegistrationRequest: async (firstName, lastName, photo, id_role, phoneNumber, email, username, password) => {
        let sqlInsert;

        if(photo === null) {
            sqlInsert = `INSERT INTO users (firstName, lastName, photo, id_role, phoneNumber, email, username, password, approved) VALUES 
            ('${firstName}', '${lastName}', ${photo}, ${id_role}, '${phoneNumber}', '${email}', '${username}', '${password}', false);`
        }

        else {
            sqlInsert = `INSERT INTO users (firstName, lastName, photo, id_role, phoneNumber, email, username, password, approved) VALUES 
            ('${firstName}', '${lastName}', '${photo}', ${id_role}, '${phoneNumber}', '${email}', '${username}', '${password}', false);`
        }

        const sqlSelect = `SELECT id_user FROM users WHERE 
                            username = '${username}';`;

    
        try {
           
            await db.query(sqlInsert, []);
            const result = await db.query(sqlSelect, []);
            return result.rows[0].id_user;
    
        } catch (err) {
            console.log(err);
            throw err
        }
    },

    correctPassword: async (username, password) => {
        const sql = `SELECT id_user FROM users WHERE 
                        username = '${username}' AND password = '${password}';`;
    
        try {
           
            const result = await db.query(sql, []);
            return result.rows == 0 ? false : true;
    
        } catch (err) {
            console.log(err);
            throw err
        }
    },

    registrationRequestApproved: async (username, password) => {
        const sql = `SELECT id_user FROM users WHERE 
                        username = '${username}' AND approved = true;`;
    
        try {
           
            const result = await db.query(sql, []);
            return result.rows == 0 ? false : true;
    
        } catch (err) {
            console.log(err);
            throw err
        }
    },

    getUserByUsername: async username => {
        const sql = `SELECT id_user, firstName, lastName, role, id_station_member FROM users NATURAL JOIN user_role WHERE 
                        username = '${username}';`;
    
        try {
           
            const result = await db.query(sql, []);
            return result.rows[0];
    
        } catch (err) {
            console.log(err);
            throw err
        }
    },

    checkIfLeadRescuer: async id_user => {
        const sql = `SELECT * FROM station 
                        WHERE id_lead_rescuer = ${id_user};`;
    
        try {
           
            const result = await db.query(sql, []);
            return result.rows[0];
    
        } catch (err) {
            console.log(err);
            throw err
        }
    }
}