const db = require(".");

module.exports = {
    
    createMission: async (description, id_station) => {
        const sqlInsert = `INSERT INTO mission (description, id_station) 
                                VALUES ( '${description}', ${id_station});`;
    
        try {
           
            const result = await db.query(sqlInsert, []);
            return result.rows;
    
        } catch (err) {
            console.log(err);
            throw err
        }
    },

    getMissions: async () => {
        const sql = `SELECT name, id_mission, description FROM mission NATURAL JOIN station;`;
    
        try {
            const result = await db.query(sql, []);
            return result.rows;
    
        } catch (err) {
            console.log(err);
            throw err
        }
    },

    deleteMission: async id_mission => {
        const sqlDeletedMission = `SELECT description FROM mission 
                                    WHERE id_mission =  ${id_mission};`;
    
        const sqlDeleteMission = `DELETE FROM mission 
                                    WHERE id_mission = ${id_mission};`;               

        try {
           
            const result = await db.query(sqlDeletedMission, []);
            await db.query(sqlDeleteMission, []);

            return result.rows[0];
    
        } catch (err) {
            console.log(err);
            throw err
        }
    },

    getMissionInfo: async id_mission => {
        const sql = `SELECT name, description 
                        FROM mission NATURAL JOIN station
                        WHERE id_mission = ${id_mission};`;
    
        try {
            const result = await db.query(sql, []);
            return result.rows;
    
        } catch (err) {
            console.log(err);
            throw err
        }
    },

    getTaskTypes: async () => {
        const sql = `SELECT * FROM task_type;`;
    
        try {
            const result = await db.query(sql, []);
            return result.rows;
    
        } catch (err) {
            console.log(err);
            throw err
        }
    },

    getTransportationTypes: async () => {
        const sql = `SELECT * FROM transportation_type;`;
    
        try {
            const result = await db.query(sql, []);
            return result.rows;
    
        } catch (err) {
            console.log(err);
            throw err
        }
    },

    createMissionTask: async (id_mission, id_task_type, id_transportation_type) => {
        const sqlInsert = `INSERT INTO mission_task (id_mission, id_task_type, id_transportation_type, isFinished) 
                                VALUES ( ${id_mission}, ${id_task_type}, ${id_transportation_type}, false);`;
    
        try {
           
            const result = await db.query(sqlInsert, []);
            return result.rows;
    
        } catch (err) {
            console.log(err);
            throw err
        }
    },

    getMissionTasks: async id_mission => {
        const sql = `SELECT id_mission, id_mission_task, isfinished, task_name, transportation_name, firstName, lastName
                        FROM mission_task NATURAL JOIN task_type NATURAL JOIN transportation_type
                        LEFT JOIN users ON id_rescuer = id_user
                        WHERE id_mission = ${id_mission};`;
    
        try {
            const result = await db.query(sql, []);
            return result.rows;
    
        } catch (err) {
            console.log(err);
            throw err
        }
    },

    finishMissionTask: async id_mission_task => {
        let sqlUpdate = `UPDATE mission_task
                            SET isfinished = true
                            WHERE id_mission_task = ${id_mission_task};`;

        try {
            await db.query(sqlUpdate, []);
            return
    
        } catch (err) {
            console.log(err);
            throw err
        }
    },
}