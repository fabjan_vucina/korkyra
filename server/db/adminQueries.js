const db = require(".");

module.exports = {
    
    getUnapprovedRegistrationRequests: async () => {
        const sql = `SELECT id_user, firstName, lastName, role, email, phoneNumber FROM users NATURAL JOIN user_role WHERE
                        approved = false;`;
    
        try {
           
            const result = await db.query(sql, []);
            return result.rows;
    
        } catch (err) {
            console.log(err);
            throw err
        }
    },

    approveRegistrationRequest: async id_user => {
        const sqlUpdate = `UPDATE users SET approved = true WHERE id_user = ${id_user}`;

        const sqlGetUpdated = `SELECT id_user, firstName, lastName FROM users WHERE
                                    id_user =  ${id_user};`;
    
        try {
           
            await db.query(sqlUpdate, []);
            const result = await db.query(sqlGetUpdated, []);

            return result.rows[0];
    
        } catch (err) {
            console.log(err);
            throw err
        }
    },

    rejectRegistrationRequest: async id_user => {
        const sqlDeletedRequest = `SELECT firstName, lastName FROM users WHERE
                                    id_user =  ${id_user} AND approved = false;`;
    
        const sqlDelete = `DELETE FROM users WHERE id_user = ${id_user} AND approved = false;`;

        
        try {
           
            const result = await db.query(sqlDeletedRequest, []);
            await db.query(sqlDelete, []);

            return result.rows[0];
    
        } catch (err) {
            console.log(err);
            throw err
        }
    },

    getAuthenticatedUsers: async () => {
        const sql = `SELECT id_user, firstName, lastName, role, email, phoneNumber FROM users NATURAL JOIN user_role WHERE
                        approved = true AND id_role <> 1;`;
    
        try {
           
            const result = await db.query(sql, []);
            return result.rows;
    
        } catch (err) {
            console.log(err);
            throw err
        }
    },

    deleteUser: async id_user => {
        const sqlDeletedRequest = `SELECT firstName, lastName FROM users WHERE
                                    id_user =  ${id_user} AND approved = true;`;
    
        const sqlDelete = `DELETE FROM users WHERE id_user = ${id_user} AND approved = true;`;

        
        try {
           
            const result = await db.query(sqlDeletedRequest, []);
            await db.query(sqlDelete, []);

            return result.rows[0];
    
        } catch (err) {
            console.log(err);
            throw err
        }
    },

    assignRescuerToStation: async (id_user, id_station_member) => {
        let sql = `UPDATE users
                        SET id_station_member = ${id_station_member}
                        WHERE id_user = ${id_user};`;
    
        try {
            await db.query(sql, []);
            return;
    
        } catch (err) {
            console.log(err);
            throw err
        }
    },

    changeRescuerInfo: async (id_user, phoneNumber, email, id_station_member) => {
        let sqlUpdate = `UPDATE users
                            SET phoneNumber = '${phoneNumber}',
                                email = '${email}',
                                id_station_member = ${id_station_member}
                            WHERE id_user = ${id_user};`;

        const sqlSelect = `SELECT firstName, lastName FROM users 
                              WHERE id_user = ${id_user};`;

    
        try {
           
            await db.query(sqlUpdate, []);
            const result = await db.query(sqlSelect, []);
            return result.rows[0];
    
        } catch (err) {
            console.log(err);
            throw err
        }
    },

    changeDispatcherInfo: async (id_user, phoneNumber, email) => {
        let sqlUpdate = `UPDATE users
                            SET phoneNumber = '${phoneNumber}',
                                email = '${email}'
                            WHERE id_user = ${id_user};`;

        const sqlSelect = `SELECT firstName, lastName FROM users 
                              WHERE id_user = ${id_user};`;

    
        try {
           
            await db.query(sqlUpdate, []);
            const result = await db.query(sqlSelect, []);
            return result.rows[0];
    
        } catch (err) {
            console.log(err);
            throw err
        }
    },

    getStations: async () => {
        const sql = `SELECT id_station, name, firstname, lastname 
                        FROM station LEFT JOIN users
                        ON id_lead_rescuer = id_user
                        ORDER BY id_station;`;
    
        try {
            const result = await db.query(sql, []);
            return result.rows;
    
        } catch (err) {
            console.log(err);
            throw err
        }
    },

    getStationRescuers: async id_station => {
        const sql = `SELECT id_user, firstname, lastname FROM users 
                        WHERE id_station_member = ${id_station} AND id_role = 2;`;
    
        try {
            const result = await db.query(sql, []);
            return result.rows;
    
        } catch (err) {
            console.log(err);
            throw err
        }
    },

    changeStationInfo: async (id_station, id_lead_rescuer) => {
        let sqlUpdate = `UPDATE station
                            SET id_lead_rescuer = ${id_lead_rescuer}
                            WHERE id_station = ${id_station};`;

        const sqlSelect = `SELECT name, firstName, lastName FROM station JOIN users
                              ON id_lead_rescuer = id_user
                              WHERE id_station = ${id_station};`;
    
        try {
           
            await db.query(sqlUpdate, []);
            const result = await db.query(sqlSelect, []);
            return result.rows[0];
    
        } catch (err) {
            console.log(err);
            throw err
        }
    },

    stationAlreadyExists: async name => {
        const sql = `SELECT name FROM station 
                        WHERE name = '${name}';`;
    
        try {
           
            const result = await db.query(sql, []);
            return result.rows == 0 ? false : true;
    
        } catch (err) {
            console.log(err);
            throw err
        }
    },

    addStation: async name => {
        const sqlInsert = `INSERT INTO station (name) VALUES ( '${name}');`;

        const sqlSelect = `SELECT id_station FROM station
                             WHERE name = '${name}';`;
    
        try {
            await db.query(sqlInsert, []);
            const result = await db.query(sqlSelect, []);
            return result.rows;
    
        } catch (err) {
            console.log(err);
            throw err
        }
    },

    deleteStation: async id_station => {
        const sqlDeletedStation = `SELECT name FROM station
                                     WHERE id_station = ${id_station};`;
    
        const sqlDelete = `DELETE FROM station WHERE id_station = ${id_station};`;
        
        try {
           
            const result = await db.query(sqlDeletedStation, []);
            await db.query(sqlDelete, []);

            return result.rows[0];
    
        } catch (err) {
            console.log(err);
            throw err
        }
    },
}