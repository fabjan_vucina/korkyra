const express = require('express')
const router = express.Router();
const dispatcherQueries = require('../db/dispatcherQueries');

router.post('/api/createMission', (req, res) => {
    (async () => {
        
        await dispatcherQueries.createMission(req.body.description, req.body.id_station);

        res.json({
            message: `You have created a new mission.`
        })

    })();
})

router.get('/api/missions', (req, res) => {
    (async () => {
        
        const existingMissions = await dispatcherQueries.getMissions();

        res.json(existingMissions)

    })();
})

router.delete('/api/deleteMission', (req, res) => {
    (async () => {
        
        const deletedMission = await dispatcherQueries.deleteMission(req.body.id_mission);

        res.json({
            message: `You have deleted the mission: ${deletedMission.description}`
        })

    })();
})

router.get('/api/missionInfo/:id_mission', (req, res) => {
    (async () => {
        
        const missionInfo = await dispatcherQueries.getMissionInfo(req.params.id_mission);

        res.json(missionInfo)

    })();
})

router.get('/api/taskTypes', (req, res) => {
    (async () => {
        
        const taskTypes = await dispatcherQueries.getTaskTypes();

        res.json(taskTypes)

    })();
})

router.get('/api/transportationTypes', (req, res) => {
    (async () => {
        
        const transportationTypes = await dispatcherQueries.getTransportationTypes();

        res.json(transportationTypes)

    })();
})

router.post('/api/createMissionTask', (req, res) => {
    (async () => {
        
        await dispatcherQueries.createMissionTask(req.body.id_mission,
                                                  req.body.id_task_type,
                                                  req.body.id_transportation_type);

        res.json({
            message: `You have created a new mission task.`
        })

    })();
})

router.get('/api/missionTasks/:id_mission', (req, res) => {
    (async () => {
        
        const missionTasks = await dispatcherQueries.getMissionTasks(req.params.id_mission);

        res.json(missionTasks)

    })();
})

router.put('/api/finishMissionTask', (req, res) => {
    (async () => {
        
        await dispatcherQueries.finishMissionTask(req.body.id_mission_task);

        res.json({
            message: `The mission task has been marked as finished.`
        })

    })();
})

module.exports = router;