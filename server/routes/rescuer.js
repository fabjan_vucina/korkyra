const express = require("express");
const router = express.Router();
const rescuerQueries = require("../db/rescuerQueries");

router.get("/api/helpRequests/:id_user", (req, res) => {
  (async () => {
    const helpRequests = await rescuerQueries.getHelpRequests(
      req.params.id_user
    );

    res.json(helpRequests);
  })();
});

router.put("/api/acceptHelpRequest", (req, res) => {
  (async () => {
    await rescuerQueries.acceptHelpRequest(
      req.body.id_user,
      req.body.id_mission_task
    );

    res.json({
      message: `You have accepted the help request.`,
    });
  })();
});

router.get("/api/participatingTasks/:id_user", (req, res) => {
  (async () => {
    const participatingTasks = await rescuerQueries.getParticipatingTasks(
      req.params.id_user
    );

    res.json(participatingTasks);
  })();
});

router.get("/api/missionColleagues/:id_user/:id_mission", (req, res) => {
  (async () => {
    const missionColleagues = await rescuerQueries.getMissionColleagues(
      req.params.id_user,
      req.params.id_mission
    );

    res.json(missionColleagues);
  })();
});

router.get("/api/missionComments/:id_mission", (req, res) => {
  (async () => {
    const missionComments = await rescuerQueries.getMissionComments(
      req.params.id_mission
    );

    res.json(missionComments);
  })();
});

router.post("/api/leaveComment", (req, res) => {
  (async () => {
    const leftComment = await rescuerQueries.leaveComment(
      req.body.id_user,
      req.body.id_mission,
      req.body.comment
    );

    res.json({
      message: `You have left the comment: ${leftComment.comment}`,
    });
  })();
});

router.delete("/api/removeTransportationEligibilities", (req, res) => {
  (async () => {
    await rescuerQueries.removeTransportationEligibilities(req.body.id_user);

    res.json({
      message: `You have removed the rescuer's transportation eligibilities.`,
    });
  })();
});

router.post("/api/assignTransportationTypes", (req, res) => {
  (async () => {
    await rescuerQueries.assignTransportationTypes(
      req.body.id_user,
      ...req.body.transportation_types
    );

    res.json({
      message: `You have assigned the transportation types to the rescuer.`,
    });
  })();
});

module.exports = router;
