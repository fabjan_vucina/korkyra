const express = require('express')
const router = express.Router();
const adminQueries = require('../db/adminQueries');

router.get('/api/registrationRequests', (req, res) => {
    (async () => {
        
        const unapprovedRegistrationRequests = await adminQueries.getUnapprovedRegistrationRequests();

        res.json(unapprovedRegistrationRequests);

    })();
})

router.put('/api/approveRequest', (req, res) => {
    (async () => {
        
        const approveRegistrationRequest = await adminQueries.approveRegistrationRequest(req.body.id_user);

        res.json({
            message: `You have approved access to ${approveRegistrationRequest.firstname} ${approveRegistrationRequest.lastname}.`
        })

    })();
})

router.delete('/api/rejectRequest', (req, res) => {
    (async () => {
        
        const rejectRegistrationRequest = await adminQueries.rejectRegistrationRequest(req.body.id_user);

        res.json({
            message: `You have rejected access to ${rejectRegistrationRequest.firstname} ${rejectRegistrationRequest.lastname}.`
        })

    })();
})

router.get('/api/authenticatedUsers', (req, res) => {
    (async () => {
        
        const authenticatedUsers = await adminQueries.getAuthenticatedUsers();

        res.json(authenticatedUsers);

    })();
})

router.delete('/api/deleteUser', (req, res) => {
    (async () => {
        
        const deletedUser = await adminQueries.deleteUser(req.body.id_user);

        res.json({
            message: `You have deleted user: ${deletedUser.firstname} ${deletedUser.lastname}.`
        })

    })();
})

router.put('/api/changeRescuerInfo', (req, res) => {
    (async () => {
        
        const changedRescuerInfo = await adminQueries.changeRescuerInfo(req.body.id_user,
                                                                 req.body.phoneNumber, 
                                                                 req.body.email,
                                                                 req.body.id_station_member);

        res.json({
            message: `You have updated rescuer: ${changedRescuerInfo.firstname} ${changedRescuerInfo.lastname}.`
        })

    })();
})

router.put('/api/changeDispatcherInfo', (req, res) => {
    (async () => {
        
        const changedDispatcherInfo = await adminQueries.changeDispatcherInfo(req.body.id_user,
                                                                 req.body.phoneNumber, 
                                                                 req.body.email);

        res.json({
            message: `You have updated user: ${changedDispatcherInfo.firstname} ${changedDispatcherInfo.lastname}.`
        })

    })();
})

router.get('/api/stations', (req, res) => {
    (async () => {
        
        const stations = await adminQueries.getStations();

        res.json(stations);

    })();
})

router.get('/api/stationRescuers/:id_station', (req, res) => {
    (async () => {
        
        const stationRescuers = await adminQueries.getStationRescuers(req.params.id_station);

        res.json(stationRescuers);

    })();
})

router.put('/api/changeStationInfo', (req, res) => {
    (async () => {
        
        const changedStationInfo = await adminQueries.changeStationInfo(req.body.id_station,
                                                                     req.body.id_lead_rescuer);

        res.json({
            message: `You have updated station: ${changedStationInfo.name}. The new lead rescuer is: ${changedStationInfo.firstname} ${changedStationInfo.lastname}.`
        })

    })();
})

router.post('/api/addStation', (req, res) => {
    (async () => {

        const stationAlreadyExists = await adminQueries.stationAlreadyExists(req.body.name);
        if(stationAlreadyExists) {
             return res.status(400).send({
                 message: 'This station already exists!'
            });
        }
        
        const addedStation = await adminQueries.addStation(req.body.name);

        res.json({
            message: `You have created a new station.`
        })

    })();
})

router.delete('/api/deleteStation', (req, res) => {
    (async () => {
        
        const deletedStation = await adminQueries.deleteStation(req.body.id_station);

        res.json({
            message: `You have deleted station: ${deletedStation.name}.`
        })

    })();
})

module.exports = router;