const express = require('express')
const router = express.Router();
const authQueries = require('../db/authQueries');

router.post('/api/registration', (req, res) => {
    (async () => {

        const usernameAlreadyExists = await authQueries.usernameAlreadyExists(req.body.username);
            if(usernameAlreadyExists) {
                return res.status(400).send({
                     message: 'Username already exists!'
                });
            }

        
        const emailAlreadyExists = await authQueries.emailAlreadyExists(req.body.email);
        if(emailAlreadyExists) {
            return res.status(400).send({
                message: 'Email already exists!'
             });
        }

        const id_user = await authQueries.createRegistrationRequest(req.body.firstName, 
                                                                req.body.lastName, 
                                                                req.body.photo, 
                                                                req.body.id_role,
                                                                req.body.phoneNumber, 
                                                                req.body.email, 
                                                                req.body.username, 
                                                                req.body.password);

        res.json({
            message: "Your registration request has been submitted."
        })

    })();
})

router.post('/api/login', (req, res) => {
    (async () => {
        
        const usernameAlreadyExists = await authQueries.usernameAlreadyExists(req.body.username);
        if(!usernameAlreadyExists) {
             return res.status(400).send({
                 message: 'This username doesn\'t exist!'
            });
        }

        
        const correctPassword = await authQueries.correctPassword(req.body.username, req.body.password);
        if(!correctPassword) {
            return res.status(400).send({
                message: `Incorrect password for username: ${req.body.username}`
             });
        }


        const registrationRequestApproved = await authQueries.registrationRequestApproved(req.body.username);
        if(!registrationRequestApproved) {
            return res.status(400).send({
                message: 'Your registration request has not yet been approved by the administrator'
             });
        }

        const user = await authQueries.getUserByUsername(req.body.username);

        //check if rescuer is lead rescuer
        let isLeadRescuer = false;
        if(user.role === "Rescuer") {
            const isLead = await authQueries.checkIfLeadRescuer(user.id_user);
            if(isLead) {
                isLeadRescuer = true;
            }
        }

        //attributes from the database are lowercase
        res.json({
            id_user: user.id_user,
            firstName: user.firstname,
            lastName: user.lastname,
            role: user.role,
            isLeadRescuer: isLeadRescuer,
            id_station_member: user.id_station_member
        })
    })();
})

module.exports = router;