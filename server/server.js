//create express server app
const express = require("express");
const app = express();

//allows access to req.body
const bodyParser = require("body-parser");
app.use(bodyParser.json());

//allow cross-origin resource sharing
const cors = require("cors");
app.use(cors());

//tells the express app where the frontend build folder is
//our server app responds to every request by sending a response with a static file from the build folder or dynamically responding with the help from the database
const path = require("path");
const buildPath = path.join(__dirname, "..", "build");
app.use(express.static(buildPath));

//import request handlers
const userRoute = require("./routes/user.js");
const adminRoute = require("./routes/admin.js");
const dispatcherRoute = require("./routes/dispatcher.js");
const rescuerRoute = require("./routes/rescuer.js");

//use request handlers
app.use(userRoute);
app.use(adminRoute);
app.use(dispatcherRoute);
app.use(rescuerRoute);

//handling post request made by the avatar component after a users uploads a photo
app.post("/api/photoUpload", (req, res) => {
  res.json("Uploaded photo");
});

//handling static GET requests for pages in production
app.use("/", (req, res) => {
  res.sendFile(path.join(buildPath, "index.html"));
});

//start listening for requests
const port = process.env.PORT || 5000;
app.listen(port, () => console.log(`Server started on port ${port}`));
