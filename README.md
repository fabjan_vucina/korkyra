<img src="documentation/img/hgsstracks_logo.png" />

## A simple web app for optimizing organization and task delegation for the HGSS.

https://hgsstracks.herokuapp.com/

Note: the layout and styling is optimized for a **1280px x 578px** viewport.

# Prerequisites to run locally

- clone this project repository
- navigate to the repo folder in your terminal
- run command `cd server`
- run command `npm run install-dependencies` to install dependencies for both the frontend and backend apps
- create a a blank test database in PostgreSQL
- in `server/db/seed.js` and `server/db/index.js`, replace the `database`, `user` and `password` fields in the `pool` variable according to your info
- run command `npm run seed`
- run command `npm run dev` to concurrently run both the frontend and backend apps

# Project team

- Fabjan Vučina
- Luka Sunara
- Josip Ivančević
- Filip Petani
- Paula Hernandez
- Pablo Felipe
- Katarina Virkes
